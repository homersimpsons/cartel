import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph';
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold';
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic';
import Underline from '@ckeditor/ckeditor5-basic-styles/src/underline';
import Strikethrough from '@ckeditor/ckeditor5-basic-styles/src/strikethrough';
import Alignment from '@ckeditor/ckeditor5-alignment/src/alignment';
import Heading from '@ckeditor/ckeditor5-heading/src/heading';
import List from '@ckeditor/ckeditor5-list/src/list';
import Link from '@ckeditor/ckeditor5-link/src/link';
import Image from '@ckeditor/ckeditor5-image/src/image';
import ImageToolbar from '@ckeditor/ckeditor5-image/src/imagetoolbar';
import ImageStyle from '@ckeditor/ckeditor5-image/src/imagestyle';
import ImageUpload from '@ckeditor/ckeditor5-image/src/imageupload';
import CKFinderUploadAdapter from '@ckeditor/ckeditor5-adapter-ckfinder/src/uploadadapter';
import Highlight from '@ckeditor/ckeditor5-highlight/src/highlight';
import BlockQuote from '@ckeditor/ckeditor5-block-quote/src/blockquote';
// @ckeditor/ckeditor5-table
// Video (Youtube at least - planned)
// Indent/Dedent
// Multiple editor onin a single page

document.querySelectorAll( '.editor' ).forEach( el => {
    ClassicEditor
        .create( el, {
            plugins: [
                Heading,
                Essentials,
                Paragraph,
                Bold,
                Italic,
                Underline,
                Strikethrough,
                Alignment,
                List,
                Link,
                Image,
                ImageToolbar,
                ImageStyle,
                ImageUpload,
                CKFinderUploadAdapter,
                Highlight,
                BlockQuote,
            ],
            toolbar: [ 
                'undo', 'redo', '|',
                'heading', '|',
                'bold', 'italic', 'underline', 'strikethrough', 'highlight', 'link', 'blockquote', '|',
                'alignment:left', 'alignment:center', 'alignment:right', 'alignment:justify', '|',
                'numberedList', 'bulletedList', '|',
                'imageUpload',
            ],
            ckfinder: {
                uploadUrl: '/upload',
            },
            image: {
                toolbar: [ 'imageTextAlternative', '|', 'imageStyle:full', 'imageStyle:side' ],
                // styles: [ 'imageStyle:full' ]
            }
        } )
        .then( editor => {
            console.log( 'Available plugins :', Array.from( editor.ui.componentFactory.names() ) );
            console.log( 'Editor was initialized', editor );
        } )
        .catch( error => {
            console.error( error.stack );
        } );
})
