'use strict';

const path = require( 'path' );
const { styles } = require( '@ckeditor/ckeditor5-dev-utils' );

module.exports = {
    // https://webpack.js.org/configuration/entry-context/
    entry: {
        ckeditor: './assets/js/ckeditor.js',
    },

    // https://webpack.js.org/configuration/output/
    output: {
        path: path.resolve( __dirname, 'public/build/js' ),
        filename: '[name].js'
    },

    module: {
        rules: [
            {
                // Or /ckeditor5-[^/]+\/theme\/icons\/[^/]+\.svg$/ if you want to limit this loader
                // to CKEditor 5's icons only.
                test: /\.svg$/,

                use: [ 'raw-loader' ]
            },
            {
                // Or /ckeditor5-[^/]+\/theme\/[^/]+\.css$/ if you want to limit this loader
                // to CKEditor 5's theme only.
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader',
                        options: {
                            singleton: true
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: styles.getPostCssConfig( {
                            themeImporter: {
                                themePath: require.resolve( '@ckeditor/ckeditor5-theme-lark' ) // @TODO not used
                            },
                            minify: true
                        } )
                    },
                ]
            }
        ]
    },

    // Useful for debugging.
    devtool: 'source-map'
};

// var Encore = require('@symfony/webpack-encore');

// Encore
//     // the project directory where compiled assets will be stored
//     .setOutputPath('public/build/')
//     // the public path used by the web server to access the previous directory
//     .setPublicPath('/build')
//     .cleanupOutputBeforeBuild()
//     .enableSourceMaps(!Encore.isProduction())
//     // uncomment to create hashed filenames (e.g. app.abc123.css)
//     // .enableVersioning(Encore.isProduction())

//     // uncomment to define the assets of the project
//     // .addEntry('js/app', './assets/js/app.js')
//     // .addStyleEntry('css/app', './assets/css/app.scss')

//     // uncomment if you use Sass/SCSS files
//     // .enableSassLoader()

//     // uncomment for legacy applications that require $/jQuery as a global variable
//     // .autoProvidejQuery()
// ;

// module.exports = Encore.getWebpackConfig();
