<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Delegations
 *
 * @ORM\Table(name="delegations")
 * @ORM\Entity(repositoryClass="App\Repository\DelegationsRepository")
 */
class Delegations
{
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Picture", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    protected $file;

    /**
     * @Assert\Image(
     *  maxSize = "1M",
     * )
     */
    protected $uFile; // Uploaded file
    
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Events", mappedBy="delegations")
     */
    private $events;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(
     *  max = 255,
     * )
     */
    private $name;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Delegations
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set uFile
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile|null $uFile
     *
     * @return Pictures
     */
    public function setUFile(\Symfony\Component\HttpFoundation\File\UploadedFile $uFile = null, $override = false)
    {
        if (!$override && !$uFile) {
            return;
        }
        $this->uFile = $uFile;
        $file = $this->getFile();
        if (!$file) {
            $file = new Picture();
        }
        $file->setFile($uFile);
        $this->setFile($file);
        
        return $this;
    }

    /**
     * Get uFile
     *
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile|null
     */
    public function getUFile()
    {
        return $this->uFile;
    }

    /**
     * Set file.
     *
     * @param \App\Entity\Picture|null $file
     *
     * @return Pictures
     */
    public function setFile(\App\Entity\Picture $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file.
     *
     * @return \App\Entity\Picture|null
     */
    public function getFile()
    {
        return $this->file;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add event
     *
     * @param \App\Entity\Events $event
     *
     * @return Delegations
     */
    public function addEvent(\App\Entity\Events $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \App\Entity\Events $event
     */
    public function removeEvent(\App\Entity\Events $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
    * toString
    *
    * @return string
    */
    public function __toString()
    {
        return (string) $this->getName();
    }
}
