<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Partners
 *
 * @ORM\Table(name="partners")
 * @ORM\Entity(repositoryClass="App\Repository\PartnersRepository")
 */
class Partners
{
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Picture", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    protected $file;
    
    /**
     * @Assert\Image(
     *  maxSize = "5M",
     * )
     */
    protected $uFile; // Uploaded file
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *  max = 255,
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *  max = 255,
     * )
     * @Assert\Url()
     */
    private $url;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Partners
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Partners
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set uFile
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile|null $uFile
     *
     * @return Pictures
     */
    public function setUFile(\Symfony\Component\HttpFoundation\File\UploadedFile $uFile = null, $override = false)
    {
        if (!$override && !$uFile) {
            return;
        }
        $this->uFile = $uFile;
        $file = $this->getFile();
        if (!$file) {
            $file = new Picture();
        }
        $file->setFile($uFile);
        $this->setFile($file);
        
        return $this;
    }

    /**
     * Get uFile
     *
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile|null
     */
    public function getUFile()
    {
        return $this->uFile;
    }

    /**
     * Set file.
     *
     * @param \App\Entity\Picture|null $file
     *
     * @return Pictures
     */
    public function setFile(\App\Entity\Picture $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file.
     *
     * @return \App\Entity\Picture|null
     */
    public function getFile()
    {
        return $this->file;
    }
}
