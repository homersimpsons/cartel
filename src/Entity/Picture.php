<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Imagine\Gd\Imagine;

/**
 * Picture
 *
 * @ORM\Entity(repositoryClass="App\Repository\PictureRepository")
 */
class Picture extends Uploads
{
    /**
     * @Assert\Image(
     *  maxSize = "5M",
     * )
     */
    protected $file;

    private $thumbPath = 'thumbs/';

    protected function preUpdateFile(){}

    protected function postUpdateFile()
    {
        if (null !== $this->tempFileName) {
            $oldFile = $this->getUploadRootDir().$this->thumbPath.$this->tempFileName;
            if (file_exists($oldFile)) {
                unlink($oldFile);
            }
        }
        if ($this->hasThumb()) {
            $imagine = new Imagine();
            $image = $imagine->open($this->getUploadRootDir().$this->getFileNameExt());
            $box = $image->getSize()->scale(min(350 / $image->getSize()->getWidth(), 200 / $image->getSize()->getHeight()));
            $image->thumbnail($box)->save($this->getUploadRootDir().$this->thumbPath.$this->getFileNameExt());
        }
    }

    protected function preRemoveFile(){}

    protected function postRemoveFile()
    {
        $oldFile = $this->getUploadRootDir().$this->thumbPath.$this->tempFileName;
        if (file_exists($oldFile)) {
            unlink($oldFile);
        }
    }

    public function getThumbUri()
    {
        if ($this->hasThumb()) {
            return $this->getUploadDir().$this->thumbPath.$this->getFileNameExt();
        }
        return $this->getFileUri();
    }
    
    public function hasThumb($ext = null)
    {
        if (!$ext) {
            $ext = $this->getExtension();
        }
        // Supported images by Gd\Imagine
        // https://github.com/avalanche123/Imagine/blob/10e9522f11a6e846061226b7e54347909a849e09/lib/Imagine/Gd/Image.php#L657
        return in_array($ext, array('gif', 'jpg', 'jpeg', 'pjpeg', 'png', 'wbmp', 'xbm'));
    }
}
