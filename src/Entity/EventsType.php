<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EventsType
 *
 * @ORM\Table(name="events_type")
 * @ORM\Entity(repositoryClass="App\Repository\EventsTypeRepository")
 */
class EventsType
{
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Events", mappedBy="eventType")
     */
    private $events;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *  max = 255,
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="render", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *  max = 255,
     * )
     */
    private $render;
    
    /**
     * @var boolean
     * @ORM\Column(name="athlete", type="boolean")
     */
    protected $athlete;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return EventsType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add event
     *
     * @param \App\Entity\Events $event
     *
     * @return EventsType
     */
    public function addEvent(\App\Entity\Events $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \App\Entity\Events $event
     */
    public function removeEvent(\App\Entity\Events $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
    * toString
    *
    * @return string
    */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Set render
     *
     * @param string $render
     *
     * @return EventsType
     */
    public function setRender($render)
    {
        $this->render = $render;

        return $this;
    }

    /**
     * Get render
     *
     * @return string
     */
    public function getRender()
    {
        return $this->render;
    }

    /**
     * Set athlete
     *
     * @param boolean $athlete
     *
     * @return EventsType
     */
    public function setAthlete($athlete)
    {
        $this->athlete = $athlete;

        return $this;
    }

    /**
     * Get athlete
     *
     * @return boolean
     */
    public function getAthlete()
    {
        return $this->athlete;
    }
}
