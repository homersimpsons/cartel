<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Sonata\TranslationBundle\Model\TranslatableInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Events
 *
 * @ORM\Table(name="events")
 * @ORM\Entity(repositoryClass="App\Repository\EventsRepository")
 */
class Events implements TranslatableInterface
{
    use ORMBehaviors\Translatable\Translatable;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Places", inversedBy="events")
     * @ORM\JoinColumn(nullable=false)
     */
    private $place;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EventsType", inversedBy="events")
     * @ORM\JoinColumn(nullable=false)
     */
    private $eventType;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sports", inversedBy="events")
     */
    private $sport;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\News", mappedBy="event")
     */
    private $news;
    
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Delegations", inversedBy="events")
     * @ORM\JoinColumn(nullable=false)
     */
    private $delegations;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Scores", mappedBy="event")
     */
    private $scores;
     
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_time", type="datetime")
     * @Assert\NotBlank()
     * @Assert\DateTime()
     * @Assert\LessThan(
     *  propertyPath = "endTime",
     * )
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_time", type="datetime")
     * @Assert\NotBlank()
     * @Assert\DateTime()
     * @Assert\GreaterThan(
     *  propertyPath = "startTime",
     * )
     */
    private $endTime;
    
    /**
     * @var boolean
     * @ORM\Column(name="homepage", type="boolean")
     */
    protected $homepage;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return Events
     */
    public function setStartTime(\DateTime $startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     *
     * @return Events
     */
    public function setEndTime(\DateTime $endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime
     */
    public function getEndTime()
    {
        return $this->endTime;
    }
    
    /**
     * Set place
     *
     * @param \App\Entity\Places $place
     *
     * @return Places
     */
    public function setPlace(\App\Entity\Places $place = null)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return \App\Entity\Places
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set homepage
     *
     * @param boolean $homepage
     *
     * @return Events
     */
    public function setHomepage($homepage)
    {
        $this->homepage = $homepage;

        return $this;
    }

    /**
     * Get homepage
     *
     * @return boolean
     */
    public function getHomepage()
    {
        return $this->homepage;
    }
    
    /**
     * Set eventType
     *
     * @param \App\Entity\EventsType $eventType
     *
     * @return Events
     */
    public function setEventType(\App\Entity\EventsType $eventType = null)
    {
        $this->eventType = $eventType;

        return $this;
    }

    /**
     * Get eventType
     *
     * @return \App\Entity\EventsType
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->delegations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setStartTime(new \DateTime);
        $this->setEndTime(new \DateTime('+1hour'));
        $this->setHomepage(false);
        $this->news = new ArrayCollection();
        $this->scores = new ArrayCollection();
    }

    /**
     * Add delegation
     *
     * @param \App\Entity\Delegations $delegation
     *
     * @return Events
     */
    public function addDelegation(\App\Entity\Delegations $delegation)
    {
        $this->delegations[] = $delegation;

        return $this;
    }

    /**
     * Remove delegation
     *
     * @param \App\Entity\Delegations $delegation
     */
    public function removeDelegation(\App\Entity\Delegations $delegation)
    {
        $this->delegations->removeElement($delegation);
    }

    /**
     * Get delegations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDelegations()
    {
        return $this->delegations;
    }

    /**
     * Set sport
     *
     * @param \App\Entity\Sports $sport
     *
     * @return Events
     */
    public function setSport(\App\Entity\Sports $sport = null)
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * Get sport
     *
     * @return \App\Entity\Sports
     */
    public function getSport()
    {
        return $this->sport;
    }

    /**
     * Add news
     *
     * @param \App\Entity\News $news
     *
     * @return Events
     */
    public function addNews(\App\Entity\News $news)
    {
        $this->news[] = $news;

        return $this;
    }

    /**
     * Remove news
     *
     * @param \App\Entity\News $news
     */
    public function removeNews(\App\Entity\News $news)
    {
        $this->news->removeElement($news);
    }

    /**
     * Get news
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * Add score
     *
     * @param \App\Entity\Scores $score
     *
     * @return Events
     */
    public function addScore(\App\Entity\Scores $score)
    {
        $this->scores[] = $score;

        return $this;
    }

    /**
     * Remove score
     *
     * @param \App\Entity\Scores $score
     */
    public function removeScore(\App\Entity\Scores $score)
    {
        $this->scores->removeElement($score);
    }

    /**
     * Get scores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScores() 
    {
        return $this->scores;
    }

    public function __call($method, $arguments)
    {
        return \Symfony\Component\PropertyAccess\PropertyAccess::createPropertyAccessor()->getValue($this->translate(), $method);
    }

    /**
    * toString
    *
    * @return string
    */
    public function __toString()
    {
        return '#'.$this->getId().' : '.$this->getName();
    }
    
    /**
     * @return mixed
     */
    public function getTName()
    {
        return $this->translate(null, false)->getName();
    }

    /**
     * @param string $name
     */
    public function setTName($name)
    {
        $this->translate(null, false)->setName($name);

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getTDescription()
    {
        return $this->translate(null, false)->getDescription();
    }

    /**
     * @param string $description
     */
    public function setTDescription($description)
    {
        $this->translate(null, false)->setDescription($description);

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getTShortDescription()
    {
        return $this->translate(null, false)->getShortDescription();
    }

    /**
     * @param string $shortDescription
     */
    public function setTShortDescription($shortDescription)
    {
        $this->translate(null, false)->setShortDescription($shortDescription);

        return $this;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->setCurrentLocale($locale);

        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->getCurrentLocale();
    }
}
