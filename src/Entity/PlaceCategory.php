<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Sonata\TranslationBundle\Model\TranslatableInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlaceCategoryRepository")
 */
class PlaceCategory implements TranslatableInterface
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Places", mappedBy="placeCategory")
     */
    private $places;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function __construct()
    {
        $this->places = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }
    public function __call($method, $arguments)
    {
        return \Symfony\Component\PropertyAccess\PropertyAccess::createPropertyAccessor()->getValue($this->translate(), $method);
    }

    /**
    * toString
    *
    * @return string
    */
    public function __toString()
    {
        return '#'.$this->getId().' : '.$this->getName();
    }

    /**
     * @return mixed
     */
    public function getTName()
    {
        return $this->translate(null, false)->getName();
    }

    /**
     * @param string $name
     */
    public function setTName($name)
    {
        $this->translate(null, false)->setName($name);

        return $this;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->setCurrentLocale($locale);

        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->getCurrentLocale();
    }

    /**
     * @return Collection|Places[]
     */
    public function getPlaces(): Collection
    {
        return $this->places;
    }

    public function addPlace(Places $place): self
    {
        if (!$this->places->contains($place)) {
            $this->places[] = $place;
            $place->setPlaceCategory($this);
        }

        return $this;
    }

    public function removePlace(Places $place): self
    {
        if ($this->places->contains($place)) {
            $this->places->removeElement($place);
            // set the owning side to null (unless already changed)
            if ($place->getPlaceCategory() === $this) {
                $place->setPlaceCategory(null);
            }
        }

        return $this;
    }
}
