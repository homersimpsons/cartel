<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Places
 *
 * @ORM\Table(name="places")
 * @ORM\Entity(repositoryClass="App\Repository\PlacesRepository")
 */
class Places
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaceCategory", inversedBy="places")
     * @ORM\JoinColumn(nullable=false)
     */
    private $placeCategory;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Shuttles", mappedBy="departurePlace")
     */
    private $fromShuttles;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Shuttles", mappedBy="arrivalPlace")
     */
    private $toShuttles;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Events", mappedBy="place")
     */
    private $events;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *  max = 255,
     * )
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *  max = 255,
     * )
     */
    private $address;

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="decimal", precision=10, scale=6)
     * @Assert\NotBlank()
     * @Assert\Range(
     *  min = -90,
     *  max = 90,
     * )
     */
    private $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="lon", type="decimal", precision=10, scale=6)
     * @Assert\NotBlank()
     * @Assert\Range(
     *  min = -180,
     *  max = 180,
     * )
     */
    private $lon;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Places
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fromShuttles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->toShuttles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add fromShuttle
     *
     * @param \App\Entity\Shuttles $fromShuttle
     *
     * @return Places
     */
    public function addFromShuttle(\App\Entity\Shuttles $fromShuttle)
    {
        $this->fromShuttles[] = $fromShuttle;

        return $this;
    }

    /**
     * Remove fromShuttle
     *
     * @param \App\Entity\Shuttles $fromShuttle
     */
    public function removeFromShuttle(\App\Entity\Shuttles $fromShuttle)
    {
        $this->fromShuttles->removeElement($fromShuttle);
    }

    /**
     * Get fromShuttles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFromShuttles()
    {
        return $this->fromShuttles;
    }

    /**
     * Add toShuttle
     *
     * @param \App\Entity\Shuttles $toShuttle
     *
     * @return Places
     */
    public function addToShuttle(\App\Entity\Shuttles $toShuttle)
    {
        $this->toShuttles[] = $toShuttle;

        return $this;
    }

    /**
     * Remove toShuttle
     *
     * @param \App\Entity\Shuttles $toShuttle
     */
    public function removeToShuttle(\App\Entity\Shuttles $toShuttle)
    {
        $this->toShuttles->removeElement($toShuttle);
    }

    /**
     * Get toShuttles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getToShuttles()
    {
        return $this->toShuttles;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Places
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set lat.
     *
     * @param float $lat
     *
     * @return Places
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat.
     *
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon.
     *
     * @param float $lon
     *
     * @return Places
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon.
     *
     * @return float
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
    * toString
    *
    * @return string
    */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Add event.
     *
     * @param \App\Entity\Events $event
     *
     * @return Places
     */
    public function addEvent(\App\Entity\Events $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event.
     *
     * @param \App\Entity\Events $event
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeEvent(\App\Entity\Events $event)
    {
        return $this->events->removeElement($event);
    }

    /**
     * Get events.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    public function getPlaceCategory()
    {
        return $this->placeCategory;
    }

    public function setPlaceCategory(PlaceCategory $placeCategory)
    {
        $this->placeCategory = $placeCategory;

        return $this;
    }
}
