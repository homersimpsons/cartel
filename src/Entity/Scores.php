<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Scores
 *
 * @ORM\Table(name="scores",uniqueConstraints={@UniqueConstraint(name="score", columns={"delegation_id", "event_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\ScoresRepository")
 */
class Scores
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Delegations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $delegation;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Events", inversedBy="scores")
     * @ORM\JoinColumn(nullable=false)
     */
    private $event;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="score", type="smallint")
     */
    private $score;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return Scores
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return int
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set delegation
     *
     * @param \App\Entity\Delegations $delegation
     *
     * @return Scores
     */
    public function setDelegation(\App\Entity\Delegations $delegation)
    {
        $this->delegation = $delegation;

        return $this;
    }

    /**
     * Get delegation
     *
     * @return \App\Entity\Delegations
     */
    public function getDelegation()
    {
        return $this->delegation;
    }

    /**
     * Set event
     *
     * @param \App\Entity\Events $event
     *
     * @return Scores
     */
    public function setEvent(\App\Entity\Events $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \App\Entity\Events
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
    * toString
    *
    * @return string
    */
    public function __toString()
    {
        return $this->getDelegation().': '.$this->getScore().' ('.$this->getEvent().')';
    }
}
