<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Picture
 *
 * @ORM\Entity(repositoryClass="App\Repository\PictureRepository")
 */
class PDF extends Uploads
{
    
    /**
     * @Assert\File(
     *  mimeTypes = {"application/pdf", "application/x-pdf"},
     *  maxSize = "5M",
     * )
     */
    protected $file;

    public function getThumbUri()
    {
        return 'img/pdf.svg';
    }

    protected function preUpdateFile(){}
    protected function postUpdateFile(){}
    protected function preRemoveFile(){}
    protected function postRemoveFile(){}
}
