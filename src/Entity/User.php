<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="email.already-used")
 */
class User implements UserInterface, AdvancedUserInterface, EquatableInterface, \Serializable
{
    const ROLE_DEFAULT = 'ROLE_USER';

    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
  
    public function __construct()
    {
        $this->setAllowPicture(1);
        $this->locked = false;
        $this->roles = array();
        $this->sports = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(
     *  max = 255,
     * )
     * @Assert\Email()
     */
    protected $email;

    /**
     * @var boolean
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    protected $locked;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=60, options={"fixed" = true})
     */
    protected $password;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    protected $lastLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="confirmation_token", type="string", length=43, unique=true, nullable=true, options={"fixed" = true})
     */
    protected $confirmationToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="password_requested_at", type="datetime", nullable=true)
     */
    protected $passwordRequestedAt;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="array")
     */
    protected $roles;
    
    /**
     * @var boolean
     * @ORM\Column(name="athlete", type="boolean")
     */
    protected $athlete;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allowPicture", type="boolean")
     */
    protected $allowPicture;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Delegations")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $delegation;
    
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Sports")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $sports;
    
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $coChamber;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *  max = 255,
     * )
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *  max = 255,
     * )
     */
    protected $lastName;

    /**
     * @var string
     *
     * @Assert\Expression(
     *  "not (false === this.getPassword() || (this.empty(this.getPassword()) && '0' != this.getPassword())) || not(false === this.getPlainPassword() || (this.empty(this.getPlainPassword()) && '0' != this.getPlainPassword()))"
     * )
     * @Assert\Length(max=4096)
     */
    protected $plainPassword;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
        ) = unserialize($serialized);
    }

    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof User) {
            return false;
        }
        if ($this->getUsername() !== $user->getUsername()) {
            return false;
        }
        return true;
    }

    public function eraseCredentials()
    {
        $this->setPlainPassword(null);
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return !$this->getLocked();
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
        if ($password) {
            $this->setPassword(null);
        }

        return $this;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getSalt() {
        return 'test';
    }

    public function getUsername() {
        return $this->getEmail();
    }

    public function addRole($role)
    {
        $role = strtoupper($role);
        if ($role === static::ROLE_DEFAULT) {
            return $this;
        }

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function getRoles()
    {
        $roles = $this->roles;

        // we need to make sure to have at least one role
        $roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }

    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    public function setSuperAdmin($boolean)
    {
        if (true === $boolean) {
            $this->addRole(static::ROLE_SUPER_ADMIN);
        } else {
            $this->removeRole(static::ROLE_SUPER_ADMIN);
        }

        return $this;
    }

    public function isSuperAdmin()
    {
        return $this->hasRole(static::ROLE_SUPER_ADMIN);
    }

    public function isAdmin()
    {
        return $this->hasRole(static::ROLE_ADMIN);
    }

    public function setRoles(array $roles)
    {
        $this->roles = array();

        foreach ($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set locked.
     *
     * @param bool $locked
     *
     * @return User
     */
    public function setLocked($locked)
    {
        $this->locked = !$this->isSuperAdmin() && $locked;

        return $this;
    }

    /**
     * Get locked.
     *
     * @return bool
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Get enabled.
     *
     * @return bool
     */
    public function isEnabled()
    {
        return !$this->getConfirmationToken() || $this->getPasswordRequestedAt();
    }

    /**
     * Set password.
     *
     * @param binary $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set lastLogin.
     *
     * @param \DateTime $lastLogin
     *
     * @return User
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Update lastLogin.
     *
     * @return User
     */
    public function updateLastLogin()
    {
        $this->setLastLogin(new \DateTime());

        return $this;
    }

    /**
     * Get lastLogin.
     *
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set confirmationToken.
     *
     * @param string $confirmationToken
     *
     * @return User
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * Get confirmationToken.
     *
     * @return string
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * Set passwordRequestedAt.
     *
     * @param \DateTime|null $passwordRequestedAt
     *
     * @return User
     */
    public function setPasswordRequestedAt($passwordRequestedAt = null)
    {
        $this->passwordRequestedAt = $passwordRequestedAt;

        return $this;
    }

    /**
     * Get passwordRequestedAt.
     *
     * @return \DateTime|null
     */
    public function getPasswordRequestedAt()
    {
        return $this->passwordRequestedAt;
    }

    /**
     * Set athlete.
     *
     * @param bool $athlete
     *
     * @return User
     */
    public function setAthlete($athlete)
    {
        $this->athlete = $athlete;

        return $this;
    }

    /**
     * Get athlete.
     *
     * @return bool
     */
    public function getAthlete()
    {
        return $this->athlete;
    }

    /**
     * Set allowPicture.
     *
     * @param bool $allowPicture
     *
     * @return User
     */
    public function setAllowPicture($allowPicture)
    {
        $this->allowPicture = $allowPicture;

        return $this;
    }

    /**
     * Get allowPicture.
     *
     * @return bool
     */
    public function getAllowPicture()
    {
        return $this->allowPicture;
    }

    /**
     * Set firstName.
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = ucwords(strtolower($firstName));

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName.
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = ucwords(strtolower($lastName));

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set delegation.
     *
     * @param \App\Entity\Delegations $delegation
     *
     * @return User
     */
    public function setDelegation(\App\Entity\Delegations $delegation)
    {
        $this->delegation = $delegation;

        return $this;
    }

    /**
     * Get delegation.
     *
     * @return \App\Entity\Delegations
     */
    public function getDelegation()
    {
        return $this->delegation;
    }

    /**
     * Add sport.
     *
     * @param \App\Entity\Sports $sport
     *
     * @return User
     */
    public function addSport(\App\Entity\Sports $sport)
    {
        $this->sports[] = $sport;

        return $this;
    }

    /**
     * Remove sport.
     *
     * @param \App\Entity\Sports $sport
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSport(\App\Entity\Sports $sport)
    {
        return $this->sports->removeElement($sport);
    }

    /**
     * Get sports.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSports()
    {
        return $this->sports;
    }

    /**
     * Set coChamber.
     *
     * @param \App\Entity\User|null $coChamber
     *
     * @return User
     */
    public function setCoChamber(\App\Entity\User $coChamber = null)
    {
        $this->coChamber = $coChamber;

        return $this;
    }

    /**
     * Get coChamber.
     *
     * @return \App\Entity\User|null
     */
    public function getCoChamber()
    {
        return $this->coChamber;
    }

    public function getArraySportsId()
    {
        $ArraySportsId = array();
        foreach ($this->sports->toArray() as $sport) {
            $ArraySportsId[] = $sport->getId();
        }
        return $ArraySportsId;
    }

    public function __toString()
    {
        return $this->getFirstName().' '.$this->getLastName();
    }

    public function empty($value)
    {
        return empty($value);
    }
}
