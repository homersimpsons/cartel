<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Pictures
 *
 * @ORM\Table(name="pictures")
 * @ORM\Entity(repositoryClass="App\Repository\PicturesRepository")
 */
class Pictures
{
    public function __construct()
    {
        $this->display = 0;
    }
    
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Picture", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    protected $file;
    
    /**
     * @Assert\Image(
     *  maxSize = "5M",
     * )
     */
    protected $uFile; // Uploaded file
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     * @ORM\Column(name="display", type="boolean")
     */
    protected $display;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50)
     * @Assert\NotBlank()
     * @Assert\Length(
     *  max = 50,
     * )
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Assert\Length(
     *  max = 65535,
     * )
     */
    private $description;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Pictures
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Pictures
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set uFile
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile|null $uFile
     *
     * @return Pictures
     */
    public function setUFile(\Symfony\Component\HttpFoundation\File\UploadedFile $uFile = null)
    {
        $this->uFile = $uFile;
        
        $file = new Picture();
        $file->setFile($uFile);
        $this->setFile($file);
        
        return $this;
    }

    /**
     * Get uFile
     *
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile|null
     */
    public function getUFile()
    {
        return $this->uFile;
    }

    /**
     * Set file.
     *
     * @param \App\Entity\Picture|null $file
     *
     * @return Pictures
     */
    public function setFile(\App\Entity\Picture $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file.
     *
     * @return \App\Entity\Picture|null
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set display.
     *
     * @param bool $display
     *
     * @return Pictures
     */
    public function setDisplay($display)
    {
        $this->display = $display;

        return $this;
    }

    /**
     * Get display.
     *
     * @return bool
     */
    public function getDisplay()
    {
        return (bool) $this->display;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return Pictures
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
