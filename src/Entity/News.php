<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Sonata\TranslationBundle\Model\TranslatableInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * News
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 */
class News implements TranslatableInterface
{
    use ORMBehaviors\Translatable\Translatable;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Events", inversedBy="news")
     * @ORM\JoinColumn(nullable=false)
     */
    private $event;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime")
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    private $time;
    
    public function __construct()
    {
        $this->setTime(new \DateTime);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return News
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set event
     *
     * @param \App\Entity\Events $event
     *
     * @return News
     */
    public function setEvent(\App\Entity\Events $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \App\Entity\Events
     */
    public function getEvent()
    {
        return $this->event;
    }

    public function __call($method, $arguments)
    {
        return \Symfony\Component\PropertyAccess\PropertyAccess::createPropertyAccessor()->getValue($this->translate(null, true), $method);
    }

    /**
    * toString
    *
    * @return string
    */
    public function __toString()
    {
        return '#'.$this->getId().' : '.$this->getTitle();
    }
    
    /**
     * @return mixed
     */
    public function getTTitle()
    {
        return $this->translate(null, false)->getTitle();
    }

    /**
     * @param string $title
     */
    public function setTTitle($title)
    {
        $this->translate(null, false)->setTitle($title);

        return $this;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->setCurrentLocale($locale);

        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->getCurrentLocale();
    }
}
