<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Shuttles
 *
 * @ORM\Table(name="shuttles")
 * @ORM\Entity(repositoryClass="App\Repository\ShuttlesRepository")
 */
class Shuttles
{
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Places", inversedBy="fromShuttles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $departurePlace;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Places", inversedBy="toShuttles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $arrivalPlace;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    private $departureTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime")
     * @Assert\NotBlank()
     * @Assert\DateTime()
     * @Assert\GreaterThan(
     *  propertyPath = "departureTime",
     * )
     */
    private $arrivalTime;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setDepartureTime(new \DateTime);
        $this->setArrivalTime(new \DateTime('+1hour'));
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set departure
     *
     * @param \DateTime $departure
     *
     * @return Shuttles
     */
    public function setDeparture($departure)
    {
        $this->departure = $departure;

        return $this;
    }

    /**
     * Get departure
     *
     * @return \DateTime
     */
    public function getDeparture()
    {
        return $this->departure;
    }

    /**
     * Set arrival
     *
     * @param \DateTime $arrival
     *
     * @return Shuttles
     */
    public function setArrival($arrival)
    {
        $this->arrival = $arrival;

        return $this;
    }

    /**
     * Get arrival
     *
     * @return \DateTime
     */
    public function getArrival()
    {
        return $this->arrival;
    }

    /**
     * Set departureTime
     *
     * @param \DateTime $departureTime
     *
     * @return Shuttles
     */
    public function setDepartureTime($departureTime)
    {
        $this->departureTime = $departureTime;

        return $this;
    }

    /**
     * Get departureTime
     *
     * @return \DateTime
     */
    public function getDepartureTime()
    {
        return $this->departureTime;
    }

    /**
     * Set arrivalTime
     *
     * @param \DateTime $arrivalTime
     *
     * @return Shuttles
     */
    public function setArrivalTime($arrivalTime)
    {
        $this->arrivalTime = $arrivalTime;

        return $this;
    }

    /**
     * Get arrivalTime
     *
     * @return \DateTime
     */
    public function getArrivalTime()
    {
        return $this->arrivalTime;
    }

    /**
     * Set departurePlace
     *
     * @param \App\Entity\Places $departurePlace
     *
     * @return Shuttles
     */
    public function setDeparturePlace(\App\Entity\Places $departurePlace = null)
    {
        $this->departurePlace = $departurePlace;

        return $this;
    }

    /**
     * Get departurePlace
     *
     * @return \App\Entity\Places
     */
    public function getDeparturePlace()
    {
        return $this->departurePlace;
    }

    /**
     * Set arrivalPlace
     *
     * @param \App\Entity\Places $arrivalPlace
     *
     * @return Shuttles
     */
    public function setArrivalPlace(\App\Entity\Places $arrivalPlace = null)
    {
        $this->arrivalPlace = $arrivalPlace;

        return $this;
    }

    /**
     * Get arrivalPlace
     *
     * @return \App\Entity\Places
     */
    public function getArrivalPlace()
    {
        return $this->arrivalPlace;
    }

    /**
    * toString
    *
    * @return string
    */
    public function __toString()
    {
        if ($this->getId()) {
            return '#'.$this->getId().' : ('.$this->getDepartureTime()->format('Y-m-d H:i:s').') '.$this->getDeparturePlace()->getName().' -> '.$this->getArrivalPlace()->getName().' ('.$this->getArrivalTime()->format('Y-m-d H:i:s').')';
        }
        return '';
    }
}
