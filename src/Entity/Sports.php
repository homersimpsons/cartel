<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Sonata\TranslationBundle\Model\TranslatableInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Sports
 *
 * @ORM\Table(name="sports")
 * @ORM\Entity(repositoryClass="App\Repository\SportsRepository")
 */
class Sports implements TranslatableInterface
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Events", mappedBy="sport")
     */
    private $events;
    
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Picture", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    protected $file;
    
    /**
     * @Assert\Image(
     *  maxSize = "5M",
     * )
     */
    protected $uFile; // Uploaded file
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add event
     *
     * @param \App\Entity\Events $event
     *
     * @return Sports
     */
    public function addEvent(\App\Entity\Events $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \App\Entity\Events $event
     */
    public function removeEvent(\App\Entity\Events $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Set uFile
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile|null $uFile
     *
     * @return Pictures
     */
    public function setUFile(\Symfony\Component\HttpFoundation\File\UploadedFile $uFile = null, $override = false)
    {
        if (!$override && !$uFile) {
            return;
        }
        $this->uFile = $uFile;
        $file = $this->getFile();
        if (!$file) {
            $file = new Picture();
        }
        $file->setFile($uFile);
        $this->setFile($file);
        
        return $this;
    }

    /**
     * Get uFile
     *
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile|null
     */
    public function getUFile()
    {
        return $this->uFile;
    }

    /**
     * Set file.
     *
     * @param \App\Entity\Picture|null $file
     *
     * @return Pictures
     */
    public function setFile(\App\Entity\Picture $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file.
     *
     * @return \App\Entity\Picture|null
     */
    public function getFile()
    {
        return $this->file;
    }

    public function __call($method, $arguments)
    {
        return \Symfony\Component\PropertyAccess\PropertyAccess::createPropertyAccessor()->getValue($this->translate(), $method);
    }

    /**
    * toString
    *
    * @return string
    */
    public function __toString()
    {
        return (string) $this->getName();
    }
    
    /**
     * @return mixed
     */
    public function getTName()
    {
        return $this->translate(null, false)->getName();
    }

    /**
     * @param string $name
     */
    public function setTName($name)
    {
        $this->translate(null, false)->setName($name);

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getTDescription()
    {
        return $this->translate(null, false)->getDescription();
    }

    /**
     * @param string $description
     */
    public function setTDescription($description)
    {
        $this->translate(null, false)->setDescription($description);

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getTInfos()
    {
        return $this->translate(null, false)->getInfos();
    }

    /**
     * @param string $infos
     */
    public function setTInfos($infos)
    {
        $this->translate(null, false)->setInfos($infos);

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getTRules()
    {
        return $this->translate(null, false)->getRules();
    }

    /**
     * @param string $rules
     */
    public function setTRules($rules)
    {
        $this->translate(null, false)->setRules($rules);

        return $this;
    }
    
    /**
     * @return mixed
     */
    public function getTProgress()
    {
        return $this->translate(null, false)->getProgress();
    }

    /**
     * @param string $progress
     */
    public function setTProgress($progress)
    {
        $this->translate(null, false)->setProgress($progress);

        return $this;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->setCurrentLocale($locale);

        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->getCurrentLocale();
    }
}
