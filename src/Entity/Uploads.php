<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Utils\UUID;

/**
 * Uploads
 *
 * @ORM\Table(name="uploads")
 * @ORM\Entity(repositoryClass="App\Repository\UploadsRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="class", type="string")
 * @ORM\DiscriminatorMap({"picture" = "Picture", "pdf" = "PDF"})
 * @ORM\HasLifecycleCallbacks
 */
abstract class Uploads
{
    protected $file;
    
    protected $tempFileName;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="fileName", type="string", length=36, unique=true, options={"fixed" = true})
     */
    private $fileName; // UUID

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=4)
     */
    private $extension;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return uploads
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set fileName.
     *
     * @param string $fileName
     *
     * @return uploads
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName.
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set extension
     *
     * @param string $extension
     *
     * @return string
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    public function getFileNameExt()
    {
        return $this->getFileName().'.'.$this->getExtension();
    }

    public function setFile(UploadedFile $file)
    {
        $this->file = $file;
        if (null !== $this->getId()) {
            $this->tempFileName = $this->getFileNameExt();
            $this->extension = null;
        }
    }
    
    public function getFile()
    {
        return $this->file;
    }
    
    /**
    * @ORM\PrePersist()
    * @ORM\PreUpdate()
    */
    public function preUpload()
    {
        if (null === $this->getFile()) {
            return;
        }
        $this->setDate(new \DateTime());
        $this->setExtension($this->getFile()->guessExtension());
        if (!$this->getFileName()){
            $this->setFileName(UUID::v4());
        }
        $this->preUpdateFile();
    }

    abstract protected function preUpdateFile();

    /**
    * @ORM\PostPersist()
    * @ORM\PostUpdate()
    */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }
        if (null !== $this->tempFileName) {
            $oldFile = $this->getUploadRootDir().$this->tempFileName;
            if (file_exists($oldFile)) {
                unlink($oldFile);
            }
        }
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->getFileNameExt()
        );
        
        $this->postUpdateFile();
    }

    abstract protected function postUpdateFile();

    /**
    * @ORM\PreRemove()
    */
    public function preRemoveUpload()
    {
        $this->tempFileName = $this->getFileNameExt();
        $this->preRemoveFile();
    }
    
    abstract protected function preRemoveFile();

    /**
    * @ORM\PostRemove()
    */
    public function removeUpload()
    {
        $oldFile = $this->getUploadRootDir().$this->tempFileName;
        if (file_exists($oldFile)) {
            unlink($oldFile);
        }
        $this->postRemoveFile();
    }
    
    abstract protected function postRemoveFile();

    public function getFileUri()
    {
        return $this->getUploadDir().$this->getFileNameExt();
    }

    public function getThumbUri() // Thumb URI
    {
    }

    public function getUploadDir()
    {
        return 'uploads/';
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../public/'.$this->getUploadDir();
    }
    
    public function __toString()
    {
        return $this->getFileNameExt();
    }
}
