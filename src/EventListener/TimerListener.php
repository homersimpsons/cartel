<?php
namespace App\EventListener;

use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Entity\User;

class TimerListener
{
    protected $router;
    protected $tokenStorage;
    protected $timerUntil;
    protected $d404Until;

    public function __construct(Router $router, TokenStorageInterface $tokenStorage, $timerUntil, $d404Until) {
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;
        $this->timerUntil  = new \DateTime($timerUntil);
        $this->d404Until = new \DateTime($d404Until);
    }

    public function onKernelRequest($event)
    {
        if (!$event->isMasterRequest() || $this->timerUntil < new \DateTime()) { return; }
        if ($this->d404Until < new \DateTime()) {
            if ($this->isAuthorisedRoute($event->getRequest()->get('_route')) || $this->isUserLogged()) {
                return;
            }
            if (!$event->getRequest()->get('_route') || $event->getRequest()->get('_route') != 'homepage') {
                (new RedirectResponse($this->router->generate('homepage')))->send();
                exit();
            }
        } else {
            (new Response('<h1 style="text-align:center;">404: Not Found</h1>', Response::HTTP_NOT_FOUND))->send();
            exit();
        }
    }

    private function isUserLogged()
    {
        return $this->tokenStorage->getToken() && $this->tokenStorage->getToken()->getUser() instanceof User;
    }

    private function isAuthorisedRoute($route)
    {
        return in_array($route, array(
            'security_login',
            'register',
            's_enable_account',
            'reset_password',
            '_wdt',
            '_profiler'
        ));
    }    
}
