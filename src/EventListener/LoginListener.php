<?php
namespace App\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

class LoginListener {

    protected $entityManager;

    public function __construct(EntityManager $entityManager){
        $this->entityManager = $entityManager;
    }

    public function onSuccessfulLogin(AuthenticationEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
        if($user instanceof AdvancedUserInterface){
            $user->updateLastLogin();
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }
    }
}
