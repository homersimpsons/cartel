<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Shuttles;
use App\Form\Model\ShuttleSearch;
use App\Form\ShuttleSearchType;

class ShuttlesController extends Controller
{
    /**
     * @Route("/shuttles/search", name="shuttles_search")
     */
    public function searchAction(Request $request)
    {
        $SHUTTLES_PER_PAGE = 1;
        $shuttles = null;
        $pagination = null;
        $shuttleSearch = new ShuttleSearch();
        $form = $this->get('form.factory')->create(ShuttleSearchType::class, $shuttleSearch, array(
            'method' => 'GET',
            'csrf_protection' => false,
        ));
        if ($form->handleRequest($request)->isSubmitted() && $form->isValid()) {
            $request->query->remove('send');
            $em = $this->getDoctrine()->getManager();
            $shuttles = $em->getRepository(Shuttles::class)->findFromTo(
                $shuttleSearch->getDeparture()->getId(),
                $shuttleSearch->getArrival()->getId(),
                $SHUTTLES_PER_PAGE,
                ($shuttleSearch->getPage() - 1) * $SHUTTLES_PER_PAGE
            );
            $pagination = array(
                'page' => $shuttleSearch->getPage(),
                'nbPages' => ceil(count($shuttles) / $SHUTTLES_PER_PAGE),
                'nomRoute' => 'shuttles_search',
                'paramsRoute' => array_merge(
                    $request->attributes->get('_route_params'),
                    $request->query->all()
                ),
            );
            $shuttleSearch->setPage(1);
            $form = $this->get('form.factory')->create(ShuttleSearchType::class, $shuttleSearch, array(
                'method' => 'GET',
                'csrf_protection' => false,
            ));
        }
        return $this->render('shuttles/search.html.twig', array(
            'form' => $form->createView(),
            'shuttles' => $shuttles,
            'pagination' => $pagination,
        ));
    }
}
