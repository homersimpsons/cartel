<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\User;
use App\Form\Model\UserLogin;
use App\Form\LoginType;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="security_login")
     */
    public function loginAction(Request $request)
    {
        $session = $request->getSession();
        $user = $this->getUser();
        if (is_object($user) && $user instanceof User) {
            $session->getFlashBag()->add('info', 'notification.user.already-logged');
            return $this->redirect($this->generateUrl('homepage'));
        }

        // get the login error if there is one
        $error = $session->get(Security::AUTHENTICATION_ERROR);
        if ($error) {
            $session->getFlashBag()->add('danger', $error->getMessage());
        }
        $session->remove(Security::AUTHENTICATION_ERROR);

        $user = new UserLogin($session->get(Security::LAST_USERNAME));

        $form = $this->createForm(LoginType::class, $user, array(
            'action' => $this->generateUrl('login_check'),
        ));

        return $this->render('security/login.html.twig', array(
                'form' => $form->createView(),
            ));
    }

    /**
     * @Route("/login_check", name="login_check")
     */
    public function loginCheckAction()
    {
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logoutAction()
    {
    }
}
