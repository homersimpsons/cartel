<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Sports;

class SportsController extends Controller
{
    /**
     * @Route("/sport/{id}", name="sport", requirements={"id"="\d+"})
     */
    public function uniqueAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $sport = $em->getRepository(Sports::class)->findWithTR($id);
        if (null === $sport) {
            throw new NotFoundHttpException("The sport #".$id." doesn't' exist.");
        }
        return $this->render('sports/unique.html.twig', array(
			'sport' => $sport,
		));
    }
}
