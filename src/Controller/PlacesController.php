<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Places;
use App\Entity\PlaceCategory;
use App\Entity\Shuttles;

class PlacesController extends Controller
{
    /**
     * @Route("/places", name="places")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $placeCategories = $em->getRepository(PlaceCategory::class)->findAllWithPlaces();
        $map['apikey'] = $this->container->getParameter('maps.apikey');
        $map['center']['lat'] = 50.3653965;
        $map['center']['lon'] = 3.0743696;
        $map['zoom'] = 14;
        $i = 0;
        foreach ($placeCategories as $placeCategory) {
            foreach ($placeCategory->getPlaces() as $place) {
                $map['markers'][$i]['lat'] = $place->getLat();
                $map['markers'][$i]['lon'] = $place->getLon();
                $map['markers'][$i]['title'] = $place->getName();
                $map['markers'][$i]['address'] = $place->getAddress();
                $map['markers'][$i]['url'] = $this->generateUrl('place', array('id' => $place->getId()));
                $i++;
            }
        }
        return $this->render('places/index.html.twig', array(
            'placeCategories' => $placeCategories,
            'map' => $map,
        ));
    }

    /**
     * @Route("/place/{id}", name="place", requirements={"id"="\d+"})
     */
    public function uniqueAction($id)
    {
        $LIMIT = 3;
        
        $em = $this->getDoctrine()->getManager();
        $place = $em->getRepository(Places::class)->find($id);
        if (null === $place) {
            throw new NotFoundHttpException("The place #".$id." doesn't exist.");
        }
        $map['apikey'] = $this->container->getParameter('maps.apikey');
        $map['center']['lat'] = $place->getLat();
        $map['center']['lon'] = $place->getLon();
        $map['zoom'] = 17;
        $map['markers'][0]['lat'] = $place->getLat();
        $map['markers'][0]['lon'] = $place->getLon();
        $map['markers'][0]['title'] = $place->getName();
        $map['markers'][0]['address'] = $place->getAddress();
        $map['markers'][0]['url'] = $this->generateUrl('place', array('id' => $place->getId()));

        $shuttlesRepository = $em->getRepository(Shuttles::class);
        $nextDepartures = $shuttlesRepository->nextDepartures($LIMIT, $place->getId());
        $nextArrivals = $shuttlesRepository->nextArrivals($LIMIT, $place->getId());
        
        return $this->render('places/unique.html.twig', array(
            'place' => $place,
            'map' => $map,
            'nextDepartures' => $nextDepartures,
            'depLength' => count($nextDepartures),
            'nextArrivals' => $nextArrivals,
            'arrLength' => count($nextArrivals),
        ));
    }
}
