<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ContactType;
use App\Form\Model\Contact;

class ContactController extends Controller
{
    /**
     * @Route("/contact", name="contact")
     */
    public function indexAction(Request $request, \Swift_Mailer $mailer)
    {
        $user = $this->getUser();
        $contact = new Contact();
        if ($user) {
            $contact->setEmail($user->getEmail());
        }
        $form = $this->get('form.factory')->create(ContactType::class, $contact);
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $mail = $form->getData();
            $message = (new \Swift_Message('[Cartel2018 - '.$mail->getField()->getSubject().'] '.$mail->getSubject()))
                ->setFrom($mail->getEmail())
                ->setTo($mail->getField()->getEmail())
                ->setBody($mail->getContent(), 'text/html');
            $mailer->send($message);
            $this->get('session')->getFlashBag()->add('info', 'notification.contact.thanks');
            return $this->redirect($request->getUri());
        }
        return $this->render('form/contact.html.twig', array(
                'form' => $form->createView()
            ));
    }
}
