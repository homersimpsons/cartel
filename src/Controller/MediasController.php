<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

use App\Form\PicturesType;
use App\Entity\Pictures;
use App\Form\CKUploadType;
use App\Entity\Picture;

class MediasController extends Controller
{
    /**
     * @Route("/gallery/{page}", name="gallery", requirements={"page"="\d+"})
     */
    public function galleryAction(Request $request, $page = 1)
    {
        $PICTURES_PER_PAGE = $this->container->getParameter('results_per_page.pictures');

        $user = $this->getUser();
        $params = array();
        
        if (null != $user && $user->getAllowPicture()) {
            $picture = new Pictures;
            $picture->setUser($user);
            $form = $this->get('form.factory')->create(PicturesType::class, $picture);
            
            if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($picture);
                $em->flush();
                
                $request->getSession()->getFlashBag()->add('info', 'notification.pictures.add');
                
                return $this->redirect($request->getUri());
            }
            $params['form'] = $form->createView();
        }

        $repository = $this->getDoctrine()
              ->getManager()
              ->getRepository(Pictures::class);
            
        $pictures = $repository->batchPictures(($page-1)*$PICTURES_PER_PAGE, $PICTURES_PER_PAGE);
        
        $pagination = array(
            'page' => $page,
            'nbPages' => ceil(count($pictures) / $PICTURES_PER_PAGE),
            'nomRoute' => 'gallery',
            'paramsRoute' => array()
        );
        
        $params['pictures'] = $pictures;
        $params['pagination'] = $pagination;

        return $this->render('medias/index.html.twig', $params);
    }

    /**
     * @Route("/pic/{id}", name="picture", requirements={"id"="\d+"})
     */
    public function picAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $picture = $em->getRepository(Pictures::class)->find($id);
        
        if (null == $picture || !$picture->getDisplay()) {
            return $this->redirect($this->generateUrl('gallery'));
        }
        
        return $this->render('medias/picture.html.twig', array(
            'picture' => $picture
        ));
    }

    // @TODO maybe add Security constraint
    public function uploadAction(Request $request)
    {
        $picture = new Picture();
        $form = $this->get('form.factory')->create(CKUploadType::class, $picture, array(
            'csrf_protection' => false,
            'allow_extra_fields' => true, // @TODO send correct CSRF
        ));
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($picture);
            $em->flush();
            
            return new JsonResponse(array(
                'uploaded' => true,
                'url' => '/'.$picture->getFileURI(),
            ));
        }
        return new JsonResponse(array(
            'uploaded' => false,
        ));
    }
}
