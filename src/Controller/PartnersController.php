<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Partners;

class PartnersController extends Controller
{
    /**
     * @Route("/partners", name="partners")
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository(Partners::class);
        
        $partners = $repository->findAll();
        
        return $this->render('partners/index.html.twig', array(
            'partners' => $partners
        ));
    }
}
