<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Sports;
use App\Entity\Delegations;

class PresentationController extends Controller
{
    /**
     * @Route("/presentation", name="presentation")
     */
    public function indexAction()
    {
        return $this->render('presentation/index.html.twig');
    }

    /**
     * @Route("/history", name="history")
     */
    public function historyAction()
    {
        return $this->render('presentation/history.html.twig');
    }

    /**
     * @Route("/values", name="values")
     */
    public function valuesAction()
    {
        return $this->render('presentation/values.html.twig');
    }

    /**
     * @Route("/note/{who}", name="note", requirements={"who"="director|mayor|presidency"})
     */
    public function noteAction($who)
    {
        return $this->render('presentation/note/'.$who.'.html.twig');
    }

    /**
     * @Route("/goals", name="goals")
     */
    public function goalsAction()
    {
        return $this->render('presentation/goals.html.twig');
    }
    
    /**
     * @Route("/sports", name="sports")
     */
    public function sportsAction()
    {
        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository(Sports::class);

        $sports = $repository->findAllWithTR();

        return $this->render('presentation/sports.html.twig', array(
            'sports' => $sports
        ));
    }

    /**
     * @Route("/schools", name="schools")
     */
    public function schoolsAction()
    {
        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository(Delegations::class);
        $schools = $repository->findAll();
        return $this->render('presentation/schools.html.twig', array(
            'schools' => $schools
        ));
    }
}
