<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;

use App\Utils\TokenGenerator;

use App\Form\Model\ChangePassword;
use App\Form\Model\Email;
use App\Form\Model\Id;
use App\Form\Model\ResetPassword;
use App\Entity\User;
use App\Form\EmailFormType;
use App\Form\ChangePasswordType;
use App\Form\DeleteType;
use App\Form\ProfileType;
use App\Form\IdType;
use App\Form\RegisterType;
use App\Form\ResetPasswordType;

class ProfileController extends Controller
{
    protected function getPublicDir()
    {
        return __DIR__.'/../../public/';
    }

    /**
     * @Route("/register", name="register")
     */
    public function registerAction(TranslatorInterface $translator, Request $request, \Swift_Mailer $mailer)
    {
        $session = $request->getSession();
        $user = $this->getUser();
        if (is_object($user) && $user instanceof User) {
            $session->getFlashBag()->add('info', 'notification.user.already-logged');
            return $this->redirect($this->generateUrl('homepage'));
        }
        $user = new User();
        $form = $this->get('form.factory')->create(RegisterType::class, $user);
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setConfirmationToken(TokenGenerator::generateToken());
            $message = $this->getActivationMail($translator, $user);
            $mailer->send($message);
            $em->persist($user);
            $em->flush();
            $request->getSession()->getFlashBag()->add('info', 'notification.user.registration');
            return $this->redirect($this->generateUrl('security_login'));
        }
        return $this->render('profile/register.html.twig', array(
                'form' => $form->createView(),
            ));
    }

    /**
     * @Route("/profile/show", name="profile_show")
     */
    public function showAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $user = $this->getUser();
        $form = null;
        $id = new Id();
        if ($user->getCoChamber()) {
            $form = $this->get('form.factory')->create(DeleteType::class);
        } else {
            $form = $this->get('form.factory')->create(IdType::class, $id);
        }
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $persist = false;
            $em = $this->getDoctrine()->getManager();
            if ($user->getCoChamber()) {
                $coChamber = $user->getCoChamber();
                $coChamber->setCoChamber(null);
                $user->setCoChamber(null);
                $persist = true;
            } else {
                if ($user->getId() == $id->getId()) {
                    $this->get('session')->getFlashBag()->add('warning', 'notification.co-chamber.no-self');
                } else {
                    $coChamber = $em->getRepository(User::class)->find($id->getId());
                    if (null == $coChamber) {
                        $this->get('session')->getFlashBag()->add('warning', 'notification.co-chamber.no-id');
                    } elseif ($coChamber->getCoChamber()) {
                        $this->get('session')->getFlashBag()->add('warning', 'notification.co-chamber.have-one');
                    } elseif (!$coChamber->isEnabled() || !$coChamber->isAccountNonLocked()) {
                        $this->get('session')->getFlashBag()->add('warning', 'notification.co-chamber.disabled');
                    } else {
                        $user->setCoChamber($coChamber);
                        $coChamber->setCoChamber($user);
                        $persist = true;
                    }
                }
            }
            if ($persist) {
                $em->persist($user);
                $em->persist($coChamber);
                $em->flush();
                return $this->redirect($this->generateUrl('profile_show'));
            }
        }
        return $this->render('profile/show.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/profile/edit", name="profile_edit")
     */
    public function editAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $user = $this->getUser();
        $form = $this->get('form.factory')->create(ProfileType::class, $user);
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info', 'notification.profile.update');
        }
        return $this->render('profile/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/profile/change-password", name="change_password")
     */
    public function changePasswordAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $user = $this->getUser();
        $password = new ChangePassword();
        $form = $this->get('form.factory')->create(ChangePasswordType::class, $password);
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $user->setPlainPassword($password->getPassword());
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info', 'notification.profile.update');
        }
        return $this->render('profile/changePassword.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/reset-password/{key}", name="reset_password", requirements={"key"="^[a-zA-Z0-9_-]{43}$"})
     */
    public function resetPasswordAction(TranslatorInterface $translator, Request $request, \Swift_Mailer $mailer, $key = null)
    {
        $session = $request->getSession();
        $user = $this->getUser();
        if (is_object($user) && $user instanceof User) {
            $session->getFlashBag()->add('info', 'notification.user.already-logged');
            return $this->redirect($this->generateUrl('homepage'));
        }
        $form = null;
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(User::class);
        if ($key) {
            $user = $repository->findOneByConfirmationToken($key);
            if (!is_object($user) || !$user instanceof User || !$user->getPasswordRequestedAt()) {
                $this->get('session')->getFlashBag()->add('danger', 'notification.profile.token-invalid');
            } else {
                if ($user->getPasswordRequestedAt() < new \DateTime('-1hour')) {
                    $this->get('session')->getFlashBag()->add('danger', 'notification.profile.token-expired');
                    $user->setConfirmationToken(null);
                    $user->setPasswordRequestedAt(null);
                    $em->persist($user);
                    $em->flush();
                } else {
                    $password = new ResetPassword();
                    $form = $this->get('form.factory')->create(ResetPasswordType::class, $password);
                    if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
                        $user->setPlainPassword($password->getPassword());
                        $user->setConfirmationToken(null);
                        $user->setPasswordRequestedAt(null);
                        $em->persist($user);
                        $em->flush();
                        $this->get('session')->getFlashBag()->add('info', 'notification.profile.update');
                        return $this->redirect($this->generateUrl('security_login'));
                    }
                    return $this->render('profile/changePassword.html.twig', array(
                        'form' => $form->createView(),
                    ));
                }
            }
        }
        $email = new Email();
        $form = $this->get('form.factory')->create(EmailFormType::class, $email);
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $user = $repository->findOneByEmail($email->getEmail());
            if (is_object($user) && $user instanceof User) {
                $message = null;
                if ($user->isEnabled()) {
                    $user->setConfirmationToken(TokenGenerator::generateToken());
                    $user->setPasswordRequestedAt(new \DateTime());
                    $em->persist($user);
                    $em->flush();
                    $message = $this->getPasswordRequestMail($translator, $user);
                } else {
                    $message = $this->getActivationMail($translator, $user);
                }
                $mailer->send($message);
            }
            $this->get('session')->getFlashBag()->add('info', 'notification.profile.reset-password');
            return $this->redirect($request->getUri());
        }
        return $this->render('profile/passwordRequest.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function enableAction($key)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(User::class);
        $user = $repository->findOneByConfirmationToken($key);
        if (!is_object($user) || !$user instanceof User) {
            $this->get('session')->getFlashBag()->add('danger', 'notification.profile.token-invalid');
            return $this->redirect($this->generateUrl('homepage'));
        }
        $user->setConfirmationToken(null);
        $em->persist($user);
        $em->flush();
        $this->get('session')->getFlashBag()->add('info', 'notification.profile.enabled');
        return $this->redirect($this->generateUrl('security_login'));
    }

    public function getActivationMail(TranslatorInterface $translator, User $user)
    {
        $message = (new \Swift_Message($translator->trans('registration.subject')))
            ->setFrom('no-reply@cartel2018.com')
            ->setTo($user->getEmail());
        $separator = $message->embed(\Swift_Image::fromPath($this->getPublicDir().'img/sep.png'));
        $logo = $message->embed(\Swift_Image::fromPath($this->getPublicDir().'img/logo.png'));
        $options = array(
            'user' => $user,
            'logo' => $logo,
            'separator' => $separator,
        );
        $message->setBody($this->renderView('mail/registration.html.twig', $options), 'text/html');
        $message->addPart($this->renderView('mail/registration.txt.twig', $options), 'text/plain');
         return $message;
    }

    public function getPasswordRequestMail(TranslatorInterface $translator, User $user)
    {
        $message = (new \Swift_Message($translator->trans('password.subject')))
            ->setFrom('no-reply@cartel2018.com')
            ->setTo($user->getEmail());
        $separator = $message->embed(\Swift_Image::fromPath($this->getPublicDir().'img/sep.png'));
        $logo = $message->embed(\Swift_Image::fromPath($this->getPublicDir().'img/logo.png'));
        $options = array(
            'user' => $user,
            'logo' => $logo,
            'separator' => $separator,
        );
        $message->setBody($this->renderView('mail/passwordRequest.html.twig', $options), 'text/html');
        $message->addPart($this->renderView('mail/passwordRequest.txt.twig', $options), 'text/plain');
        return $message;
    }
}
