<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Events;

class IndexController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        if (!$this->getUser()) {
            $timerUntil = new \DateTime($this->container->getParameter('timerUntil'));
            if (new \DateTime() < $timerUntil) {
                return $this->render('timer.html.twig', array(
                    'timerUntil' => $timerUntil
                ));
            }
        }
        $repository = $this->getDoctrine()
                           ->getManager()
                           ->getRepository(Events::class);
        $event = $repository->featured();
        return $this->render('index.html.twig', array(
            'event' => $event,
        ));
    }

    public function dateAction()
    {
        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
