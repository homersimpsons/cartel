<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;

class RulesController extends Controller
{
    const HEADING_OFFSET = 2;
    private $translator;
    private $catalogue;
    /**
     * @Route("/rules", name="rules")
     */
    public function indexAction(TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->catalogue = $translator->getcatalogue('fr');
        $rules = $this->buildTree(array('rules'), array());
        return $this->render('rules/index.html.twig', array(
            'rules' => $rules,
        ));
    }

    private function buildTree($key, $title)
    {
        $rules = array();
        if ($this->existsInCatalogue($key, 'title')){
            $level = count($title);
            $rules['title'] = array(
                'render' => 'rules/component/title.html.twig',
                'value' => array(
                    'id' => implode('.', $key),
                    'level' => $level + self::HEADING_OFFSET,
                    'class' => $this->translator->trans('rules.level.'.$level.'.class'),
                    'string' =>
                        $this->translator->trans('rules.level.'.$level.'.prefix')
                        .' '.$this->indexCounter($title).'. '
                        .$this->translator->trans(implode('.', $key).'.title')
                        ,
                ),
            );
        }
        $i = 1;
        $loop = true;
        array_push($title, 0);
        while ($loop) {
            array_push($key, $i);
            if ($this->existsInCatalogue($key)) {
                $rules[$i] = array(
                    'render' => 'rules/component/string.html.twig',
                    'value' => $this->translator->trans(implode('.', $key)),
                );
            } else if ($this->existsInCatalogue($key, 'title')) {
                $title[count($title) - 1]++;
                $rules[$i] = array(
                    'render' => 'rules/component/section.html.twig',
                    'value' => $this->buildTree($key, $title),
                );
            } else if ($this->existsInCatalogue($key, '1')) {
                $rules[$i] = array(
                    'render' => 'rules/component/list.html.twig',
                    'value' => $this->buildTree($key, $title),
                );
            } else if ($this->existsInCatalogue($key, 'render')) {
                $rules[$i] = array(
                    'render' => $this->translator->trans(implode('.', $key).'.render'),
                    'value' => null,
                );
            } else { $loop = false; }
            array_pop($key);
            $i++;
        }
        array_pop($title);
        return $rules;
    }
    private function existsInCatalogue($key, $ext = null)
    {
        return $this->catalogue->has(implode('.', $key).($ext ? '.'.$ext : ''));
    }
    private function indexCounter($key)
    {
        $res = array();
        foreach ($key as $level => $number) {
            $res[] = $this->numberToRepresentation(
                $number,
                $this->translator->trans('rules.level.'.($level+1).'.num')   
            );
        }
        return implode('.', $res);
    }
    public function numberToRepresentation($number, $representation)
    {
        switch ($representation) {
            case 'I': return $this->numberToRomanRepresentation($number);
            case 1: return $number;
            case 'a': return array('', 'a', 'b', 'c', 'd', 'e', 'f')[$number];
        }
    }
    public function numberToRomanRepresentation($number)
    {
        $map = array('X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }
}
