<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\News;

class NewsController extends Controller
{
    /**
     * @Route("/news/{page}", name="news", requirements={"page"="\d+"})
     */
    public function indexAction($page = 1)
    {
        $NEWS_PER_PAGE = $this->container->getParameter('results_per_page.news');
        $offset = ($page - 1) * $NEWS_PER_PAGE;
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository(News::class)->latestNews($NEWS_PER_PAGE, $user, $offset);
        $pagination = array(
            'page' => $page,
            'nbPages' => ceil(count($news) / $NEWS_PER_PAGE),
            'nomRoute' => 'news',
            'paramsRoute' => array()
        );
        return $this->render('news/index.html.twig', array(
            'news' => $news,
            'pagination' => $pagination,
        ));
    }
}
