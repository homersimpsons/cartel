<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use App\Entity\Places;
use App\Entity\Events;
use App\Entity\News;
use App\Entity\Pictures;
use App\Entity\Sports;

class SitemapController extends Controller
{
    public function indexAction()
    {
        $EVENTS_PER_PAGE = $this->container->getParameter('results_per_page.events');
        $PICTURES_PER_PAGE = $this->container->getParameter('results_per_page.pictures');
        $em = $this->getDoctrine()->getManager();
        $urls = array();
    
        $places = $em->getRepository(Places::class)->findAll();
        $events = $em->getRepository(Events::class)->findAll();
        $now = new \DateTime();
        $past = 0;
        $curr = 0;
        $next = 0;
        foreach ($events as $event) {
            if ($event->getEndTime() < $now) {
                $past++;
            } else {
                if ($event->getStartTime() < $now) {
                    $curr++;
                } else {
                    $next++;
                }
            }
        }
        $news = $em->getRepository(News::class)->findAll();
        $pictures = $em->getRepository(Pictures::class)->findBy(array('display' => 1));
        $sports = $em->getRepository(Sports::class)->findAll();

        foreach ($this->container->getParameter('locales') as $locale) {
            $urls[] = $this->getUriWithTranslations('homepage', $locale);
            $urls[] = $this->getUriWithTranslations('presentation', $locale);
            $urls[] = $this->getUriWithTranslations('history', $locale);
            $urls[] = $this->getUriWithTranslations('values', $locale);
            $urls[] = $this->getUriWithTranslations('goals', $locale);
            foreach (array('director', 'mayor', 'presidency') as $who) {
                $urls[] = $this->getUriWithTranslations('note', $locale, array('who' => $who));
            }
            $urls[] = $this->getUriWithTranslations('schools', $locale);
            $urls[] = $this->getUriWithTranslations('sports', $locale);
            $urls[] = $this->getUriWithTranslations('contact', $locale);
            $urls[] = $this->getUriWithTranslations('partners', $locale);
            foreach ($places as $place) {
                $urls[] = $this->getUriWithTranslations('place', $locale, array('id' => $place->getId()));
            }
            $urls[] = $this->getUriWithTranslations('places', $locale);
            $urls[] = $this->getUriWithTranslations('shuttles_search', $locale);
            $urls[] = $this->getUriWithTranslations('events', $locale);
            foreach ($events as $event) {
                $urls[] = $this->getUriWithTranslations('event', $locale, array('id' => $event->getId()));
            }
            for ($i = 0; $i < count($news); $i++) {
                $urls[] = $this->getUriWithTranslations('news', $locale, array('page' => $i + 1));
            }
            foreach ($pictures as $picture) {
                $urls[] = $this->getUriWithTranslations('picture', $locale, array('id' => $picture->getId()));
            }
            for ($i = 0; $i < count($pictures) / $PICTURES_PER_PAGE; $i++) {
                $urls[] = $this->getUriWithTranslations('gallery', $locale, array('page' => $i + 1));
            }
            foreach ($sports as $sport) {
                $urls[] = $this->getUriWithTranslations('sport', $locale, array('id' => $sport->getId()));
            }
            $urls[] = $this->getUriWithTranslations('security_login', $locale);
            $urls[] = $this->getUriWithTranslations('register', $locale);
        }
    
        return $this->render('sitemap.xml.twig', array('urls' => $urls));
    }
  
    public function getUriWithTranslations($route, $locale, $options = array())
    {
        $url = array();
        $options['_locale'] = $locale;
        $url['loc'] = $this->generateUrl($route, $options, UrlGeneratorInterface::ABSOLUTE_URL);
        $url['links'] = array();
        foreach ($this->container->getParameter('locales') as $alternate) {
            if ($alternate != $locale) {
                $options['_locale'] = $alternate;
                $url['links'][] = array(
                    'locale' => $alternate,
                    'url' => $this->generateUrl($route, $options, UrlGeneratorInterface::ABSOLUTE_URL),
                );
            }
        }
        return $url;
    }
}
