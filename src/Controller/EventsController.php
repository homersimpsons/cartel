<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Events;
use App\Form\Model\FullCalendar;
use App\Form\FullCalendarType;

class EventsController extends Controller
{
    /**
     * @Route("/events", name="events")
     */
    public function indexAction()
    {
        return $this->render('events/index.html.twig');
    }

    /**
     * @Route("/event/{id}", name="event", requirements={"id"="\d+"})
     */
    public function uniqueAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository(Events::class)->findScore($id);
        if (null === $event) {
            throw new NotFoundHttpException("The event #".$id." doesn't' exist.");
        }
        $shuttles = null;
        if ($event->getStartTime() < new \DateTime()) { // @TODO
            // $shuttles = $em->getRepository('Shuttles')->findScore($id);
        }
        return $this->render('events/unique.html.twig', array(
                        'event' => $event,
                        'shuttles' => $shuttles,
                    ));
    }

    /**
     * @Route("/event_feed", name="event_feed")
     */
    public function eventFeedAction(Request $request)
    {
        // Full Calendar Form @TODO
        $fullCalendar = new FullCalendar();
        $form = $this->get('form.factory')->create(FullCalendarType::class, $fullCalendar, array(
            'method' => 'GET',
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ));
        $events = array();
        if ($form->handleRequest($request) && $form->isValid()) {
            $repository = $this->getDoctrine()
                ->getManager()
                ->getRepository(Events::class);
            $qEvents = $repository->findBetweenDates(
                $fullCalendar->getStart(),
                $fullCalendar->getEnd(),
                $this->getUser()
            );
            foreach ($qEvents as $event) {
                $events[] = array(
                    'title' => $event->getName(),
                    'start' => $event->getStartTime()->format("Y-m-d H:i:s"),
                    'end' => $event->getEndTime()->format("Y-m-d H:i:s"),
                    'url' => $this->generateUrl('event', array('id' => $event->getId())),
                    'className' => 'bg-'.$event->getEventType()->getRender(),
                );
            }
        }
        return new JsonResponse($events);
    }
}
