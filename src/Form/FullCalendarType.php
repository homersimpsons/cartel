<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class FullCalendarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('start', TextType::class)
            ->add('end', TextType::class);
        $builder->get('start')
            ->addModelTransformer($this->JSToPHPDateCallback());
        $builder->get('end')
            ->addModelTransformer($this->JSToPHPDateCallback());
    }

    public function getName()
    {
        return 'FullCalendar';
    }

    public function getBlockPrefix()
    {
        return '';
    }

    private function JSToPHPDateCallback()
    {
        return new CallbackTransformer(
            function ($date) {
                return $date ? $date->format('T-m-dTH:i:s') : null;
            },
            function ($string) {
                return new \DateTime(str_replace('T', ' ', $string));
            }
        );
    }
}
