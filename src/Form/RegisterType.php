<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

use App\Entity\Delegations;
use App\Entity\Sports;
use App\Entity\User;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', RepeatedType::class, array(
                    'type' => EmailType::class,
                    'first_options'  => array('label' => 'form.profile.email'),
                    'second_options' => array('label' => 'form.profile.repeat-email'),
                ))
                ->add('plainPassword', RepeatedType::class, array(
                    'type' => PasswordType::class,
                    'first_options'  => array('label' => 'form.profile.password'),
                    'second_options' => array('label' => 'form.profile.repeat-password'),
                ))
                ->add('firstName', TextType::class, array(
                    'label' => 'form.profile.firstname'
                ))
                ->add('lastName', TextType::class, array(
                    'label' => 'form.profile.lastname'
                ))
                ->add('delegation', EntityType::class, array(
                    'class' => Delegations::class,
                    'label' => 'form.profile.delegation'
                ))
                ->add('sports', EntityType::class, array(
                    'class' => Sports::class,
                    'multiple' => true,
                    'expanded' => true,
                    'label' => 'form.profile.sports'
                ))
                ->add('athlete', CheckboxType::class, array(
                    'label' => 'athlete',
                    'required' => false,
                    'label' => 'form.profile.athlete'
                ))
                ->add('sumbit', SubmitType::class, array(
                    'label' => 'form.profile.register'
                ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }

    public function getName()
    {
        return 'user_register';
    }
}
