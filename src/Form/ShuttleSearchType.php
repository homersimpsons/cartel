<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\Places;

class ShuttleSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('departure', EntityType::class, array(
                    'label' => 'form.shuttle.departure',
                    'class' => Places::class,
                ))
                ->add('arrival', EntityType::class, array(
                  'label' => 'form.shuttle.arrival',
                  'class' => Places::class,
                ))
                ->add('page', HiddenType::class)
                ->add('send', SubmitType::class, array(
                    'label' => 'form.shuttle.send',
                    'attr' => array('name' => ''),
                ));
    }

    public function getName()
    {
        return 'shuttle_search';
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
