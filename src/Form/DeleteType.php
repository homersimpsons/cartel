<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class DeleteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('delete', SubmitType::class, array(
          'label' => 'X',
          'attr' => array(
            'class' => 'btn btn-danger',
          ),
        ));
    }

    public function getName()
    {
        return 'Delete';
    }
}
