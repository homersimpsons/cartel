<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use App\Entity\Sports;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('sports', EntityType::class, array(
            'class' => Sports::class,
            'multiple' => true,
            'expanded' => true,
            'label' => 'form.profile.sports',
        ))
        ->add('athlete', CheckboxType::class, array(
            'label' => 'form.profile.athlete',
            'required' => false
        ))
        ->add('submit', SubmitType::class, array(
          'label' => 'form.profile.update',
        ));
    }

    public function getBlockPrefix()
    {
        return 'profile';
    }
}
