<?php
namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class Contact
{
    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    private $field;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *  min = 5,
     *  max = 78,
     * )
     */
    private $subject;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *  min = 15,
     *  max = 65535,
     * )
     */
    private $content;

    public function __construct()
    {
    }
      
    public function getEmail()
    {
        return $this->email;
    }
    
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    
    public function getField()
    {
        return $this->field;
    }
    
    public function setField(\App\Entity\MailSubject $field)
    {
        $this->field = $field;
        return $this;
    }
    
    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }
    
    public function getContent()
    {
        return $this->content;
    }
    
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }
}
