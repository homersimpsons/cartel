<?php
namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class ShuttleSearch
{
    private $departure;
    private $arrival;
    /**
     * @Assert\NotBlank()
     * @Assert\GreaterThan(0)
     */
    private $page = 1;

    public function getPage()
    {
        return $this->page;
    }

    public function setPage($page)
    {
        $this->page = (int) $page;

        return $this;
    }

    public function getDeparture()
    {
        return $this->departure;
    }
    
    public function setDeparture(\App\Entity\Places $departure)
    {
        $this->departure = $departure;
        return $this;
    }
    
    public function getArrival()
    {
        return $this->arrival;
    }
    
    public function setArrival(\App\Entity\Places $arrival)
    {
        $this->arrival = $arrival;
        return $this;
    }
}
