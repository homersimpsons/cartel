<?php

namespace App\Form\Model;

use Symfony\Component\Security\Core\Validator\Constraints as Assert;

class UserLogin
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *  max = 255,
     * )
     * @Assert\Email()
     */
    protected $_username;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    protected $_password;
    
    public function __construct($_username)
    {
        $this->setUsername($_username);
    }

    /**
     * Set _username.
     *
     * @param string $_username
     *
     * @return UserLogin
     */
    public function setUsername($_username)
    {
        $this->_username = $_username;

        return $this;
    }

    /**
     * Get _username.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->_username;
    }

    /**
     * Set _password.
     *
     * @param string $_password
     *
     * @return UserLogin
     */
    public function setPassword($_password)
    {
        $this->_password = $_password;

        return $this;
    }

    /**
     * Get _password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->_password;
    }
}
