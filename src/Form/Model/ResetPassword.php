<?php
namespace App\Form\Model;

use Symfony\Component\Security\Core\Validator\Constraints as Assert;

class ResetPassword
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    protected $password;

    /**
     * Set password.
     *
     * @param string $password
     *
     * @return ResetPassword
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}
