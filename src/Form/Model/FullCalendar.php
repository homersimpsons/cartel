<?php
namespace App\Form\Model;

class FullCalendar
{
    private $start;
    private $end;
    public function getStart()
    {
        return $this->start;
    }
    
    public function setStart($start)
    {
        $this->start = $start;
        return $this;
    }

    public function getEnd()
    {
        return $this->end;
    }
    
    public function setEnd($end)
    {
        $this->end = $end;
        return $this;
    }
}
