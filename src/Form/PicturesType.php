<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use App\Entity\Pictures;

class PicturesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, array(
                    'label' => 'form.picture.title'
                ))
                ->add('description', TextareaType::class, array(
                    'label' => 'form.picture.description',
                    'required' => false,
                ))
                ->add('uFile', FileType::class, array(
                    'label' => 'form.picture.file',
                ))
                ->add('send', SubmitType::class, array(
                    'label' => 'form.picture.send'
                ));
    }
 
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Pictures::class,
        ));
    }

    public function getName()
    {
        return 'Pictures';
    }
}
