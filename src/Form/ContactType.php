<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\MailSubject;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, array(
                    'label' => 'form.contact.email'
                ))
                ->add('field', EntityType::class, array(
                    'label' => 'form.contact.title',
                    'class' => MailSubject::class,
                ))
                ->add('subject', TextType::class, array(
                    'label' => 'form.contact.subject'
                ))
                ->add('content', TextareaType::class, array(
                    'label' => 'form.contact.content'
                ))
                ->add('send', SubmitType::class, array(
                    'label' => 'form.contact.send'
                ));
    }

    public function getName()
    {
        return 'Contact';
    }
}
