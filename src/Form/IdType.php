<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class IdType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', NumberType::class, array(
            'scale' => 0,
            'attr' => array(
                'min' => 0,
            ),
        ))
        ->add('send', SubmitType::class, array(
            'label' => 'form.co-chamber.submit',
        ));
    }

    public function getName()
    {
        return 'CoChamber';
    }
}
