<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;

use App\Entity\MailSubject;

class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('_username', EmailType::class, array(
                    'label' => 'form.profile.email'
                ))
                ->add('_password', PasswordType::class, array(
                    'label' => 'form.profile.password',
                ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'form.profile.connect',
                ));
    }

    public function getName()
    {
        return 'Login';
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
