<?php

namespace App\Repository;

use App\Entity\PlaceCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PlaceCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlaceCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlaceCategory[]    findAll()
 * @method PlaceCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceCategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PlaceCategory::class);
    }

    public function findAllWithPlaces()
    {
        $queryBuilder = $this->createQueryBuilder('pc');
        $queryBuilder->innerJoin('pc.places', 'p')
            ->addSelect('p');
        $queryBuilder->innerJoin('pc.translations', 'pct')
            ->addSelect('pct');
        return $queryBuilder->getQuery()->getResult();
    }
}
