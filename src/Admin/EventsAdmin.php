<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\Places;
use App\Entity\Delegations;
use App\Entity\EventsType;
use App\Entity\Sports;

class EventsAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $security = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        $isEditor = $security->isGranted('ROLE_EDITOR') || $security->isGranted('ROLE_ADMIN_EVENTS_ALL');
        $formMapper->with('Contenu')
            ->add('tName', TextType::class)
            ->add('tShortDescription', TextType::class)
            ->add('tDescription', TextareaType::class, array(
              'attr' => array('class' => 'editor'),
              'data' => $this->getSubject()->getTDescription() ? $this->getSubject()->getTDescription() : ' ',
            ))
          ->end()
          ->with('Paramètres')
            ->add('place', EntityType::class, array(
              'class' => Places::class,
              'disabled' => !$isEditor,
            ))
            ->add('delegations', EntityType::class, array(
              'class' => Delegations::class,
              'multiple' => true,
              'disabled' => !$isEditor,
              'expanded' => true,
            ))
            ->add('startTime', DateTimeType::class, array(
              'disabled' => !$isEditor,
            ))
            ->add('endTime', DateTimeType::class, array(
              'disabled' => !$isEditor,
            ))
            ->add('eventType', EntityType::class, array(
              'class' => EventsType::class,
              'disabled' => !$isEditor,
            ))
            ->add('sport', EntityType::class, array(
              'class' => Sports::class,
              'required' => false,
              'disabled' => !$isEditor,
            ))
            ->add('homepage', CheckboxType::class, array(
              'help' => 'Afficher sur la page d\'accueil',
              'required' => false,
              'disabled' => !$isEditor,
            ))
          ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $security = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        $isEditor = $security->isGranted('ROLE_EDITOR') || $security->isGranted('ROLE_ADMIN_EVENTS_ALL');
        $listMapper->addIdentifier('id')
          ->addIdentifier('tName')
          ->add('tShortDescription')
          ->add('place');
          if ($isEditor) {
            $listMapper->add('homepage', 'boolean', array('editable' => true));
          } else {
            $listMapper->add('homepage', 'boolean');
          }
    }

    public function toString($object)
    {
        return null == $object
            ? 'Events'
            : '#'.$object->getId();
    }
}
