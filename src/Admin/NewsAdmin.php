<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\Events;

class NewsAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $security = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        $isEditor = $security->isGranted('ROLE_EDITOR') || $security->isGranted('ROLE_ADMIN_NEWS_ALL');
        $formMapper->add('tTitle', TextType::class)
            ->add('time', DateTimeType::class, array(
                'disabled' => !$isEditor,
            ))
            ->add('event', EntityType::class, array(
                'class' => Events::class,
                'disabled' => !$isEditor,
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('event');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
          ->add('event')
          ->add('tTitle');
    }

    public function toString($object)
    {
        return null == $object
            ? 'News'
            : '#'.$object->getId();
    }
}
