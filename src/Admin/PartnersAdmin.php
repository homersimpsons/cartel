<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class PartnersAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class)
            ->add('url', UrlType::class)
            ->add('uFile', FileType::class, array(
                'required' => !$this->getSubject()->getFile(),
            ));
        if ($this->getSubject()->getFile()) {
            $formMapper->add('file', null, array(
                'disabled' => true,
                'help' => "<img src='".$this->getConfigurationPool()->getContainer()->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$this->getSubject()->getFile()->getFileUri()."' style='max-width:100%;'/>",
                'data_class' => null,
            ));
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
    }

    public function toString($object)
    {
        return null == $object
            ? 'Partners'
            : $object->getName();
    }
}
