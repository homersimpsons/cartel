<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\Places;

class ShuttlesAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('departurePlace', EntityType::class, array(
            'class' => Places::class,
          ))
          ->add('arrivalPlace', EntityType::class, array(
            'class' => Places::class,
          ))
          ->add('departureTime', DateTimeType::class)
          ->add('arrivalTime', DateTimeType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
        ->add('departurePlace')
        ->add('departureTime')
        ->add('arrivalPlace')
        ->add('arrivalTime');
    }

    public function toString($object)
    {
        return null == $object
            ? 'Shuttles'
            : '#'.$object->getId();
    }
}
