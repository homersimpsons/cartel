<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\PlaceCategory;

class PlacesAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class)
            ->add('address', TextType::class)
            ->add('lat', NumberType::class, array(
                'scale' => 6,
                'attr' => array(
                    'min' => -90,
                    'max' => 90,
                ),
            ))
            ->add('lon', NumberType::class, array(
                'scale' => 6,
                'attr' => array(
                    'min' => -180,
                    'min' => 180,
                ),
            ))
            ->add('placeCategory', EntityType::class, array(
                'class' => PlaceCategory::class,
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name')
            ->add('address');
    }

    public function toString($object)
    {
        return null == $object
            ? 'Places'
            : $object->getName();
    }
}
