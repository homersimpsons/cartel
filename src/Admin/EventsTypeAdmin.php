<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EventsTypeAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class)
            ->add('render', ChoiceType::class, array(
                'choices' => array(
                    'primary (blue)' => 'primary',
                    'secondary (gray)' => 'secondary',
                    'success (green)' => 'success',
                    'danger (red)' => 'danger',
                    'info (cyan)' => 'info',
                    'dark (black)' => 'dark',
                ),
                'choice_attr' => function($val, $key, $index) {
                    return ['class' => 'text-white bg-'.$val];
                },
            ))
            ->add('athlete', CheckboxType::class, array(
                'help' => 'Les athlètes doivent-ils voir ces événements ?',
                'required' => false,
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name')
            ->add('render')
            ->add('athlete');
    }
}
