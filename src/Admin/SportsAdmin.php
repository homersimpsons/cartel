<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class SportsAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('tName', TextType::class)
          ->add('tDescription', TextareaType::class, array(
            'attr' => array('class' => 'editor'),
            'data' => $this->getSubject()->getTDescription() ? $this->getSubject()->getTDescription() : ' ',
          ))
          ->add('tInfos', TextareaType::class, array(
            'attr' => array('class' => 'editor'),
            'data' => $this->getSubject()->getTInfos() ? $this->getSubject()->getTInfos() : ' ',
          ))
          ->add('tRules', TextareaType::class, array(
            'attr' => array('class' => 'editor'),
            'data' => $this->getSubject()->getTRules() ? $this->getSubject()->getTRules() : ' ',
          ))
          ->add('tProgress', TextareaType::class, array(
            'attr' => array('class' => 'editor'),
            'data' => $this->getSubject()->getTProgress() ? $this->getSubject()->getTProgress() : ' ',
          ))
          ->add('uFile', FileType::class, array(
              'required' => !$this->getSubject()->getFile(),
          ));
          if ($this->getSubject()->getFile()) {
              $formMapper->add('file', null, array(
                  'disabled' => true,
                  'help' => "<img src='".$this->getConfigurationPool()->getContainer()->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$this->getSubject()->getFile()->getFileUri()."' style='max-width:100%;'/>",
                  'data_class' => null,
              ));
          }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
    }

    public function toString($object)
    {
        return null == $object
            ? 'Sports'
            : $object->getName();
    }
}
