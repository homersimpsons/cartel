<?php
namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PicturesAdmin extends AbstractAdmin
{
    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection->remove('create');
    }
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('user', TextType::class, array('disabled' => true))
            ->add('title', TextType::class)
            ->add('description', TextareaType::class)
            ->add('display');
        if ($this->getSubject()->getFile()) {
            $formMapper->add('file', null, array(
                'disabled' => true,
                'help' => "<img src='".$this->getConfigurationPool()->getContainer()->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$this->getSubject()->getFile()->getFileUri()."' style='max-width:100%;'/>",
                'data_class' => null,
            ));
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title')
          ->add('display');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
          ->addIdentifier('title')
          ->add('description')
          ->add('display', 'boolean', array('editable' => true))
          ->add('file', null, array('template' => 'admin/list_image.html.twig'));
    }

    public function toString($object)
    {
        return null == $object
            ? 'Pictures'
            : '#'.$object->getId().': '.$object->getTitle();
    }
}
