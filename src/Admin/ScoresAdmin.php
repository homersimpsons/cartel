<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\Delegations;
use App\Entity\Events;

class ScoresAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $security = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        $isEditor = $security->isGranted('ROLE_EDITOR') || $security->isGranted('ROLE_ADMIN_SCORES_ALL');
        $formMapper->add('delegation', EntityType::class, array(
            'class' => Delegations::class,
            'disabled' => !$isEditor,
          ))
          ->add('event', EntityType::class, array(
            'class' => Events::class,
            'disabled' => !$isEditor,
          ))
          ->add('score', NumberType::class, array(
            'scale' => 0,
          ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('delegation')
          ->add('event');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
          ->add('score', 'integer', array('editable' => true))
          ->add('delegation')
          ->add('event');
    }

    public function toString($object)
    {
        return null == $object
            ? 'Events'
            : '#'.$object->getId();
    }
}
