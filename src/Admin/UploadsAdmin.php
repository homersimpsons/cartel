<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class UploadsAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('fileName', TextType::class, array(
                'disabled' => true,
            ))
            ->add('file', FileType::class)
            ->add('fileUri', TextType::class, array(
                'attr' => array('readonly' => true),
                'mapped' => false,
                'data' => '/'.$this->getSubject()->getFileUri(),
            ))
            ->add('thumbUri', TextType::class, array(
                'attr' => array('readonly' => true),
                'mapped' => false,
                'data' => '/'.$this->getSubject()->getThumbUri(),
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('fileName')
            ->add('extension');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('fileName')
          ->add('extension')
          ->add('file', null, array('template' => 'admin/list_image.html.twig'));
    }

    public function toString($object)
    {
        return null == $object
            ? 'Uploads'
            : $object->getFileName();
    }
}
