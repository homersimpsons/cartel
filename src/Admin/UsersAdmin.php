<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\Delegations;

class UsersAdmin extends AbstractAdmin
{
    public function getExportFields()
    {
        return array(
            'id',
            'email',
            'firstName',
            'lastName',
            'delegation.name',
            'athlete',
            'coChamber.email',
            'coChamber.firstName',
            'coChamber.lastName',
            'locked',
            'enabled',
        );
    }
    
    protected function configureRoutes(\Sonata\AdminBundle\Route\RouteCollection $collection)
    {
        $collection->remove('create')
            ->remove('delete');
    }
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $security = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        $formMapper->add('email', EmailType::class, array(
                'disabled' => true,
            ))
            ->add('firstName', TextType::class, array(
                'disabled' => true,
            ))
            ->add('lastName', TextType::class, array(
                'disabled' => true,
            ))
            ->add('delegation', EntityType::class, array(
                'disabled' => true,
                'class' => Delegations::class,
            ))
            ->add('athlete', CheckboxType::class, array(
                'disabled' => true,
                'required' => false,
            ))
            ->add('locked', CheckboxType::class, array(
                'required' => false,
                'disabled' => $this->getSubject()->isSuperAdmin(),
            ))
            ->add('allowPicture', CheckboxType::class, array(
                'required' => false,
            ));
        if ($security->isGranted('ROLE_ADMIN')) {
            $formMapper->add('roles', ChoiceType::class, array(
                'choices' => array(
                    'ROLE_ADMIN (droits A sur tout)' => 'ROLE_ADMIN',
                    'ROLE_DELEGATIONS (droits A sur les délégations)' => 'ROLE_DELEGATIONS',
                    'ROLE_EVENTS (droits A sur les événements)' => 'ROLE_EVENTS',
                    'ROLE_EVENTS_TYPE (droits A sur les types d\'événements)' => 'ROLE_EVENTS_TYPE',
                    'ROLE_MAIL_SUBJECT (droits A sur les types Mail Subject)' => 'ROLE_MAIL_SUBJECT',
                    'ROLE_NEWS (droits A sur les news)' => 'ROLE_NEWS',
                    'ROLE_PARTNERS (droits A sur les partenaires)' => 'ROLE_PARTNERS',
                    'ROLE_PICTURES (droits A sur les pictures)' => 'ROLE_PICTURES',
                    'ROLE_PLACE_CATEGORY (droits A sur les place category)' => 'ROLE_PLACE_CATEGORY',
                    'ROLE_PLACES (droits A sur les places)' => 'ROLE_PLACES',
                    'ROLE_SCORES (droits A sur les scores)' => 'ROLE_SCORES',
                    'ROLE_SHUTTLES (droits A sur les navettes)' => 'ROLE_SHUTTLES',
                    'ROLE_SPORTS (droits A sur les sports)' => 'ROLE_SPORTS',
                    'ROLE_EDITOR (droits L/V/C/E sur les news et événements, droits L/C sur les scores)' => 'ROLE_EDITOR',
                    'ROLE_TRANSLATOR (droits L/E sur les textes des news et événements)' => 'ROLE_TRANSLATOR',
                    'ROLE_MODERATOR (peut activer/désactiver les images et les utilisateurs)' => 'ROLE_MODERATOR',
                    'ROLE_REFEREE (peut juste modifier la valeur des scores)' => 'ROLE_REFEREE',
                ),
                'multiple' => true,
                'expanded' => true,
                'disabled' => $this->getSubject()->isSuperAdmin(),
                'help' => 'Les différents droits sont: L:List V:View C:Create E:Edit D:Delete Ex:Expoter A:All',
            ));
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('email');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('email')
            ->add('firstName')
            ->add('lastName')
            ->add('locked', 'boolean', array('editable' => true))
            ->add('enabled', 'boolean');
    }

    public function toString($object)
    {
        return null == $object
            ? 'Users'
            : $object->getEmail();
    }
}
