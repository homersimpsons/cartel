<?php

namespace App\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\News;

use App\DataFixtures\ORM\EventsFixtures;

class NewsFixtures extends Fixture implements DependentFixtureInterface
{
    // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
    public function load(ObjectManager $manager)
    {
        $night = new News();
        $night->setTime(new \DateTime('+6day'));
        $night->setEvent($this->getReference('e_night'));
        $night->translate('fr')->setTitle('Yeah !!!');
        $night->mergeNewTranslations();
        $manager->persist($night);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            EventsFixtures::class,
        );
    }
}
