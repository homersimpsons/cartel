<?php

namespace App\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\EventsType;

class EventsTypeFixtures extends Fixture
{
    // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
    public function load(ObjectManager $manager)
    {
        $sport = new EventsType();
        $sport->setName('Sport');
        $sport->setRender('success');
        $sport->setAthlete(true);
        $manager->persist($sport);
        $night = new EventsType();
        $night->setName('Night');
        $night->setRender('info');
        $night->setAthlete(true);
        $manager->persist($night);
        $activity = new EventsType();
        $activity->setName('Activity');
        $activity->setRender('warning');
        $activity->setAthlete(false);
        $manager->persist($activity);

        $manager->flush();

        $this->addReference('et_sport', $sport);
        $this->addReference('et_night', $night);
        $this->addReference('et_activity', $activity);
    }
}
