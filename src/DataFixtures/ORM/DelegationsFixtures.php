<?php

namespace App\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\Delegations;

use App\DataFixtures\ORM\PictureFixtures;

class DelegationsFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $douai = new Delegations();
        $douai->setName('Douai');
        $douai->setFile($this->getReference('picture0'));
        $manager->persist($douai);
        $paris = new Delegations();
        $paris->setName('Paris');
        $paris->setFile($this->getReference('picture1'));
        $manager->persist($paris);
        $ales = new Delegations();
        $ales->setName('Alès');
        $ales->setFile($this->getReference('picture2'));
        $manager->persist($ales);
        $albi = new Delegations();
        $albi->setName('Albi');
        $albi->setFile($this->getReference('picture3'));
        $manager->persist($albi);
        $saintE = new Delegations();
        $saintE->setName('Saint-Étienne');
        $saintE->setFile($this->getReference('picture4'));
        $manager->persist($saintE);
        $nantes = new Delegations();
        $nantes->setName('Nantes');
        $nantes->setFile($this->getReference('picture5'));
        $manager->persist($nantes);

        $manager->flush();

        $this->addReference('douai', $douai);
        $this->addReference('paris', $paris);
        $this->addReference('ales', $ales);
        $this->addReference('albi', $albi);
        $this->addReference('saintE', $saintE);
        $this->addReference('nantes', $nantes);
    }

    public function getDependencies()
    {
        return array(
            PictureFixtures::class,
        );
    }
}
