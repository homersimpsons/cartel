<?php

namespace App\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\Sports;

use App\DataFixtures\ORM\PictureFixtures;

class SportsFixtures extends Fixture implements DependentFixtureInterface
{
    // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
    public function load(ObjectManager $manager)
    {
        $football = new Sports();
        $football->setFile($this->getReference('picture0'));
        $football->translate('fr')->setName('Football');
        $football->translate('fr')->setDescription('Le football est un sport collectif qui se joue principalement au pied avec un ballon sphérique. Il oppose deux équipes de onze joueurs dans un stade, que ce soit sur un terrain gazonné ou sur un plancher. L\'objectif de chaque camp est de mettre le ballon dans le but adverse, sans utiliser les bras, et de le faire plus souvent que l\'autre équipe.');
        $football->translate('fr')->setInfos('<b>Durée:</b> 10 <b>min</b>');
        $football->translate('fr')->setRules('');
        $football->translate('fr')->setProgress('');
        $football->translate('en')->setName('Soccer');
        $football->translate('en')->setDescription('Soccer is a team sport played between two teams of eleven players with a spherical ball. It is played by 250 million players in over 200 countries and dependencies, making it the world\'s most popular sport. The game is played on a rectangular field with a goal at each end. The object of the game is to score by getting the ball into the opposing goal.');
        $football->translate('en')->setInfos('<b>Duration:</b> 10 <b>min</b>');
        $football->translate('en')->setRules('');
        $football->translate('en')->setProgress('');
        $football->mergeNewTranslations();
        $manager->persist($football);
        $handball = new Sports();
        $handball->setFile($this->getReference('picture1'));
        $handball->translate('fr')->setName('Handball');
        $handball->translate('fr')->setDescription('Le Handball est un sport collectif où deux équipes de sept joueurs s\'affrontent avec un ballon sur un terrain rectangulaire de dimensions 40 m par 20 m, séparé en deux camps');
        $handball->translate('fr')->setInfos('');
        $handball->translate('fr')->setRules('');
        $handball->translate('fr')->setProgress('');
        $handball->mergeNewTranslations();
        $manager->persist($handball);
        $rugby = new Sports();
        $rugby->setFile($this->getReference('picture2'));
        $rugby->translate('fr')->setName('Rugby');
        $rugby->translate('fr')->setDescription('Le Rugby est un sport de contact collectif chaque équipe n\'a qu\'un seul objectif: marquer le plus de points');
        $rugby->translate('fr')->setInfos('');
        $rugby->translate('fr')->setRules('');
        $rugby->translate('fr')->setProgress('');
        $rugby->mergeNewTranslations();
        $manager->persist($rugby);
        $tennis = new Sports();
        $tennis->setFile($this->getReference('picture3'));
        $tennis->translate('fr')->setName('Tennis');
        $tennis->translate('fr')->setDescription('Le Tennis est un sport de raquette où il faut taper dans une petite balle jaune');
        $tennis->translate('fr')->setInfos('');
        $tennis->translate('fr')->setRules('');
        $tennis->translate('fr')->setProgress('');
        $tennis->mergeNewTranslations();
        $manager->persist($tennis);
        $volleyball = new Sports();
        $volleyball->setFile($this->getReference('picture4'));
        $volleyball->translate('fr')->setName('Volley');
        $volleyball->translate('fr')->setDescription('Le Volley est un sport collectif mettant en jeu deux équipes de six joueurs séparés par un filet, qui s\'affrontent avec un ballon sur un terrain rectangulaire de 18 mètres de long sur 9 mètres de large. Avec 269 millions de pratiquants, il s\'agit d\'un des sports les plus pratiqués dans le monde.');
        $volleyball->translate('fr')->setInfos('');
        $volleyball->translate('fr')->setRules('');
        $volleyball->translate('fr')->setProgress('');
        $volleyball->mergeNewTranslations();
        $manager->persist($volleyball);
        $escalade = new Sports();
        $escalade->setFile($this->getReference('picture5'));
        $escalade->translate('fr')->setName('Escalade');
        $escalade->translate('fr')->setDescription('L\'Escalade est un sport individuel consitant à se déplacer le long d\'une paroi pour atteindre le haut d\'un relief ou d\'une structure artificielle, par un cheminement appelé voie et avec ou sans aide de matériel');
        $escalade->translate('fr')->setInfos('');
        $escalade->translate('fr')->setRules('');
        $escalade->translate('fr')->setProgress('');
        $escalade->mergeNewTranslations();
        $manager->persist($escalade);

        $manager->flush();

        $this->addReference('football', $football);
        $this->addReference('handball', $handball);
        $this->addReference('rugby', $rugby);
        $this->addReference('tennis', $tennis);
        $this->addReference('volleyball', $volleyball);
        $this->addReference('escalade', $escalade);
    }

    public function getDependencies()
    {
        return array(
            PictureFixtures::class,
        );
    }
}
