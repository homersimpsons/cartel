<?php

namespace App\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\Places;

use App\DataFixtures\ORM\PlaceCategoryFixtures;

class PlacesFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $MEUD = new Places();
        $MEUD->setName('MEUD');
        $MEUD->setAddress('263 rue du grand Bail, 59500, Douai');
        $MEUD->setLat(50.3653965);
        $MEUD->setLon(3.0743696);
        $MEUD->setPlaceCategory($this->getReference('pc_accomodation'));
        $manager->persist($MEUD);

        $manager->flush();

        $this->addReference('MEUD', $MEUD);
    }

    public function getDependencies()
    {
        return array(
            PlaceCategoryFixtures::class,
        );
    }
}
