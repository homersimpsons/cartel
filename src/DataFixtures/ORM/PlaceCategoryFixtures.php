<?php

namespace App\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\PlaceCategory;

class PlaceCategoryFixtures extends Fixture
{
    // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
    public function load(ObjectManager $manager)
    {
        $accomodation = new PlaceCategory();
        $accomodation->translate('fr')->setName('Hébergement');
        $accomodation->mergeNewTranslations();
        $manager->persist($accomodation);

        $manager->flush();

        $this->addReference('pc_accomodation', $accomodation);
    }
}
