<?php

namespace App\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\User;

use App\DataFixtures\ORM\DelegationsFixtures;
use App\DataFixtures\ORM\SportsFixtures;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin->setEmail('admin@example.com');
        $admin->setDelegation($this->getReference('douai'));
        $admin->setPlainPassword('admin');
        $admin->setRoles(array('ROLE_SUPER_ADMIN'));
        $admin->setAthlete(false);
        $admin->setAllowPicture(true);
        $admin->setFirstName('admin');
        $admin->setLastName('admin');
        $admin->setLocked(false);
        $admin->addSport($this->getReference('football'));
        $manager->persist($admin);
        $editor = new User();
        $editor->setEmail('editor@example.com');
        $editor->setDelegation($this->getReference('douai'));
        $editor->setPlainPassword('editor');
        $editor->setRoles(array('ROLE_EDITOR'));
        $editor->setAthlete(false);
        $editor->setAllowPicture(true);
        $editor->setFirstName('editor');
        $editor->setLastName('editor');
        $editor->setLocked(false);
        $manager->persist($editor);
        $translator = new User();
        $translator->setEmail('translator@example.com');
        $translator->setDelegation($this->getReference('douai'));
        $translator->setPlainPassword('translator');
        $translator->setRoles(array('ROLE_TRANSLATOR'));
        $translator->setAthlete(false);
        $translator->setAllowPicture(true);
        $translator->setFirstName('translator');
        $translator->setLastName('translator');
        $translator->setLocked(false);
        $manager->persist($translator);
        $moderator = new User();
        $moderator->setEmail('moderator@example.com');
        $moderator->setDelegation($this->getReference('douai'));
        $moderator->setPlainPassword('moderator');
        $moderator->setRoles(array('ROLE_MODERATOR'));
        $moderator->setAthlete(false);
        $moderator->setAllowPicture(true);
        $moderator->setFirstName('moderator');
        $moderator->setLastName('moderator');
        $moderator->setLocked(false);
        $manager->persist($moderator);
        $user = new User();
        $user->setEmail('user@example.com');
        $user->setDelegation($this->getReference('douai'));
        $user->setPlainPassword('user');
        $user->setRoles(array('ROLE_USER'));
        $user->setAthlete(false);
        $user->setAllowPicture(true);
        $user->setFirstName('user');
        $user->setLastName('user');
        $user->setLocked(false);
        $manager->persist($user);
        $referee = new User();
        $referee->setEmail('referee@example.com');
        $referee->setDelegation($this->getReference('douai'));
        $referee->setPlainPassword('referee');
        $referee->setRoles(array('ROLE_REFEREE'));
        $referee->setAthlete(false);
        $referee->setAllowPicture(true);
        $referee->setFirstName('referee');
        $referee->setLastName('referee');
        $referee->setLocked(false);
        $manager->persist($referee);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            DelegationsFixtures::class,
            SportsFixtures::class,
        );
    }
}
