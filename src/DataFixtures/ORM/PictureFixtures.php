<?php

namespace App\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Entity\Picture;

class PictureFixtures extends Fixture
{
    private function generateNewFile()
    {
        $fixturesPath = __DIR__.'/../../../public/fixtures/';
        $rnd = uniqid();
        copy($fixturesPath. 'sample.png', $fixturesPath. $rnd.'.png');
        $file = new UploadedFile($fixturesPath.$rnd.'.png', 'Image1', null, null, null, true);
        return $file;
    }
  
    public function load(ObjectManager $manager)
    {
        $picture0 = new Picture();
        $picture0->setFile($this->generateNewFile());
        $manager->persist($picture0);
        $picture1= new Picture();
        $picture1->setFile($this->generateNewFile());
        $manager->persist($picture1);
        $picture2 = new Picture();
        $picture2->setFile($this->generateNewFile());
        $manager->persist($picture2);
        $picture3 = new Picture();
        $picture3->setFile($this->generateNewFile());
        $manager->persist($picture3);
        $picture4 = new Picture();
        $picture4->setFile($this->generateNewFile());
        $manager->persist($picture4);
        $picture5 = new Picture();
        $picture5->setFile($this->generateNewFile());
        $manager->persist($picture5);
        $picture6 = new Picture();
        $picture6->setFile($this->generateNewFile());
        $manager->persist($picture6);
        $picture7 = new Picture();
        $picture7->setFile($this->generateNewFile());
        $manager->persist($picture7);
        $picture8 = new Picture();
        $picture8->setFile($this->generateNewFile());
        $manager->persist($picture8);
        $picture9 = new Picture();
        $picture9->setFile($this->generateNewFile());
        $manager->persist($picture9);

        $manager->flush();

        $this->addReference('picture0', $picture0);
        $this->addReference('picture1', $picture1);
        $this->addReference('picture2', $picture2);
        $this->addReference('picture3', $picture3);
        $this->addReference('picture4', $picture4);
        $this->addReference('picture5', $picture5);
        $this->addReference('picture6', $picture6);
        $this->addReference('picture7', $picture7);
        $this->addReference('picture8', $picture8);
        $this->addReference('picture9', $picture9);
    }
}
