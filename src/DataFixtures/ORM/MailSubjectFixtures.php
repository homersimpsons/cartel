<?php

namespace App\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\MailSubject;

class MailSubjectFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $partenaire = new MailSubject();
        $partenaire->setSubject('[Partenaire]');
        $partenaire->setEmail('zen1027214@clipmail.eu');
        $manager->persist($partenaire);
        $siteProblem = new MailSubject();
        $siteProblem->setSubject('[Problème Site]');
        $siteProblem->setEmail('zen1027214@clipmail.eu');
        $manager->persist($siteProblem);
        $eventProblem = new MailSubject();
        $eventProblem->setSubject('[Problème Événement]');
        $eventProblem->setEmail('zen1027214@clipmail.eu');
        $manager->persist($eventProblem);
        $question = new MailSubject();
        $question->setSubject('[Question]');
        $question->setEmail('zen1027214@clipmail.eu');
        $manager->persist($question);

        $manager->flush();
    }
}
