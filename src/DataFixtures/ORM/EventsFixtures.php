<?php

namespace App\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\Events;

use App\DataFixtures\ORM\DelegationsFixtures;
use App\DataFixtures\ORM\EventsTypeFixtures;
use App\DataFixtures\ORM\SportsFixtures;
use App\DataFixtures\ORM\PlacesFixtures;

class EventsFixtures extends Fixture implements DependentFixtureInterface
{
    // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
    public function load(ObjectManager $manager)
    {
        $night = new Events();
        $night->setEventType($this->getReference('et_night'));
        $night->setSport(null);
        $night->setStartTime(new \DateTime('+5day'));
        $night->setEndTime(new \DateTime('+6day'));
        $night->addDelegation($this->getReference('douai'));
        $night->setPlace($this->getReference('MEUD'));
        $night->setHomepage(true);
        $night->translate('fr')->setName('Grosse soirée !');
        $night->translate('fr')->setShortDescription('La soirée du cartel est là !');
        $night->translate('fr')->setDescription('LA vraie description : La soirée du cartel est là !');
        $night->mergeNewTranslations();
        $manager->persist($night);
        $start = new Events();
        $start->setEventType($this->getReference('et_sport'));
        $start->setSport(null);
        $start->setStartTime(new \DateTime('+5day'));
        $start->setEndTime((new \DateTime('+5day'))->modify('+1hour'));
        $start->addDelegation($this->getReference('douai'));
        $start->setPlace($this->getReference('MEUD'));
        $start->translate('fr')->setName('Lancement !');
        $start->translate('fr')->setShortDescription('Début du cartel');
        $start->translate('fr')->setDescription('C\'est le début du cartel !');
        $start->translate('en')->setName('Start !');
        $start->translate('en')->setShortDescription('Cartel Beginnings');
        $start->translate('en')->setDescription('Cartel starts !');
        $start->mergeNewTranslations();
        $manager->persist($start);

        $manager->flush();

        $this->addReference('e_night', $night);
        $this->addReference('e_start', $start);
    }

    public function getDependencies()
    {
        return array(
            DelegationsFixtures::class,
            EventsTypeFixtures::class,
            SportsFixtures::class,
            PlacesFixtures::class,
        );
    }
}
